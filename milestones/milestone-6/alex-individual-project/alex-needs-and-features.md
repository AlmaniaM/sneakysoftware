### Need & Features
	Needs 
* Database to store tasks and users
* Dropbox or Google Drive API for android
* Android phone
* App to create tasks

Features
* Create tasks to help users manage their phone calls
* Ignore call from certain people
* Ignore calls based on time of day
* Ignore calls based on certain conditions such as phone state
* Send message to user if you missed their call 
* Turn off silent mode if important person calling
* Automatically execute tasks 
* Upload tasks to Dropbox or Google Drive
* Sync calendar to execute tasks base on calendar events
* Select people to ignore or not to ignore from a list of contacts
* A Date picker/Time picker to select dates
* Display created tasks in a list
* Edit tasks that have been previously created

### Identification of Risks
	Cloud Uploads & Downloads
* It may be difficult to implement the selection of a folder to upload to
* A user could possibly upload a task and modify the contents by accident and corrupt the file. 
*  The Room database might not be fast enough to query the database and execute a task in time.
* Syncing the calendar could be difficult 




### Requirements 
	Functional
* Page to create tasks must save tasks to database
* Broadcast listener to listen for incoming calls
* Calendar and time pickers to select dates
* API for Dropbox or Google Drive to upload tasks to in an XML format or data stream.
* Must have access to users contacts to store their information such as first name, last name, phone #, possibly email and photo.
Non-Functional:
* Database must be fast enough to query data and pass it back to an object to determine if the user calling is to be ignored or not.
* The object that queries the database and cross-references the data must be optimized for quick performance to execute tasks quick.

### Release Plan: Jun 15, 2018
