﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using class_project.Models;

namespace class_project.Controllers
{
    public class RunsController : Controller
    {
        private ClassProjectContext db = new ClassProjectContext();

        // GET: Runs
        public ActionResult Index()
        {
            var runs = db.Runs.Include(r => r.Athlete).Include(r => r.RunFile);
            return View(runs.ToList());
        }

        // GET: Runs/Details/5
        public ActionResult Details(int? RunID, int? AthID, int? FileID)
        {
            if (RunID == null || AthID == null || FileID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Run run = db.Runs.Find(RunID, AthID, FileID);
            if (run == null)
            {
                return HttpNotFound();
            }
            return View(run);
        }

        // GET: Runs/Create
        public ActionResult Create()
        {
            ViewBag.AthleteID = new SelectList(db.Athletes, "AthleteID", "FirstName");
            ViewBag.FileID = new SelectList(db.RunFiles.Where(rf => rf.AthleteID == (db.Athletes.FirstOrDefault().AthleteID)), "FileID", "FitFilePath");
            return View();
        }

        [HttpGet]
        public JsonResult UpdateFileIDList(string athID)
        {
            int id = Int32.Parse(athID);

            var fileIDs = new SelectList(db.RunFiles.Where(rf => rf.AthleteID == id), "FileID", "FitFilePath").ToList();
            ViewBag.AthleteID = fileIDs;

            return Json(fileIDs, JsonRequestBehavior.AllowGet);
        }

        // POST: Runs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "RunID,AthleteID,FileID,Distance,Time,AvgHR,MaxHR")] Run run)
        {
            run.RunID = getRunID(db);
            if (ModelState.IsValid)
            {
                db.Runs.Add(run);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AthleteID = new SelectList(db.Athletes, "AthleteID", "FirstName", run.AthleteID);
            ViewBag.FileID = new SelectList(db.RunFiles, "FileID", "FitFilePath", run.FileID);
            return View(run);
        }

        private int getRunID(ClassProjectContext db)
        {
            var ids = db.Runs.Select(r => r.RunID).ToList();
            int min = 2000000;
            int max = 10000000;

            Random random = new Random();
            int runID = random.Next(min, max);

            while (ids.Contains(runID))
                runID = random.Next(min, max);

            return runID;
        }

        // GET: Runs/Edit/5
        public ActionResult Edit(int? RunID, int? AthID, int? FileID)
        {
            if (RunID == null || AthID == null || FileID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Run run = db.Runs.Find(RunID, AthID, FileID);
            if (run == null)
            {
                return HttpNotFound();
            }
            ViewBag.AthleteID = new SelectList(db.Athletes, "AthleteID", "FirstName", run.AthleteID);
            ViewBag.FileID = new SelectList(db.RunFiles, "FileID", "FitFilePath", run.FileID);
            ViewBag.Name = $"{run.Athlete.FirstName} {run.Athlete.LastName}";
            return View(run);
        }

        // POST: Runs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "RunID,AthleteID,FileID,Distance,Time,AvgHR,MaxHR")] Run run)
        {
            if (ModelState.IsValid)
            {
                db.Entry(run).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AthleteID = new SelectList(db.Athletes, "AthleteID", "FirstName", run.AthleteID);
            ViewBag.FileID = new SelectList(db.RunFiles, "FileID", "FitFilePath", run.FileID);
            return View(run);
        }

        // GET: Runs/Delete/5
        public ActionResult Delete(int? RunID, int? AthID, int? FileID)
        {
            if (RunID == null || AthID == null || FileID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Run run = db.Runs.Find(RunID, AthID, FileID);
            if (run == null)
            {
                return HttpNotFound();
            }
            return View(run);
        }

        // POST: Runs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? RunID, int? AthID, int? FileID)
        {
            Run run = db.Runs.Find(RunID, AthID, FileID);
            db.Runs.Remove(run);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
