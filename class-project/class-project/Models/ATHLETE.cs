namespace class_project.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Athlete
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Athlete()
        {
            RunFiles = new HashSet<RunFile>();
            Runs = new HashSet<Run>();
            Guardians = new HashSet<Guardian>();
        }

        public int AthleteID { get; set; }

        [Required]
        [StringLength(50)]
        [Display (Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(100)]
        [Display (Name = "Last Name")]
        public string LastName { get; set; }

        [DisplayFormat (DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime DOB { get; set; }

        public double? Weight { get; set; }

        public double? Height { get; set; }

        public int Grade { get; set; }

        [Required]
        [StringLength(50)]
        public string Team { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RunFile> RunFiles { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Run> Runs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Guardian> Guardians { get; set; }
    }
}
