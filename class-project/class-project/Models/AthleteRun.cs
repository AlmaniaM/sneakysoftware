﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace class_project.Models
{
    public class AthleteRun
    {
        public Athlete athlete { get; set; }

        public List<Run> runs { get; set; }

        public Run mostRecentRun;

        public AthleteRun (Athlete athlete)
        {
            this.athlete = athlete;
            runs = athlete.Runs.ToList();
            mostRecentRun = athlete.Runs.FirstOrDefault();
        }
    }
}