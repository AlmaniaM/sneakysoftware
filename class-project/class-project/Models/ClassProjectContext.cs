namespace class_project.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ClassProjectContext : DbContext
    {
        public ClassProjectContext()
            : base("name=ClassProjectContext")
        {
        }

        public virtual DbSet<Athlete> Athletes { get; set; }
        public virtual DbSet<Guardian> Guardians { get; set; }
        public virtual DbSet<RunFile> RunFiles { get; set; }
        public virtual DbSet<Run> Runs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Athlete>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<Athlete>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<Athlete>()
                .Property(e => e.Team)
                .IsUnicode(false);

            modelBuilder.Entity<Athlete>()
                .HasMany(e => e.Guardians)
                .WithMany(e => e.Athletes)
                .Map(m => m.ToTable("AthleteGuardians").MapLeftKey("AthleteID").MapRightKey("GID"));

            modelBuilder.Entity<Guardian>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<Guardian>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<RunFile>()
                .Property(e => e.FitFilePath)
                .IsUnicode(false);

            modelBuilder.Entity<RunFile>()
                .HasMany(e => e.Runs)
                .WithRequired(e => e.RunFile)
                .WillCascadeOnDelete(false);
        }
    }
}
