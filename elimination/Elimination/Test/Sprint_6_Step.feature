﻿Feature: Sprint_6_Step
	In order to complete sprint 6
	As a CS student
	I want to make sure all my work is correct

@RegisterWithRecaptchaSuccess
Scenario: Registering with Recaptcha success
	Given I have entered correct information to register
	And I have passed the reCaptcha
	When I press register
	Then the result should register me and taske me to the dashboard

@RegisterWithRecaptchaSFail
Scenario: Registering with Recaptcha fail
	Given I have entered correct information to register
	And I have not passed the reCaptcha
	When I press register
	Then the result should display in red letters that the reCaptcha has failed

@RegisterWithPhotoPicker
Scenario: Register using file explorer
	Given I select a photo using the file explorer
	And It's a photo of my face
	When I press register
	Then I should be registered and taken to the dashboard

@RegisterWithThreePhotos
Scenario: Register using file explorer and three photos
	Given I select 3 photos using the file explorer
	And It's a photo of my face
	When I press register
	Then I should be registered and taken to the dashboard

@RegisterWithPhotoAndDeletePhoto
Scenario: Register using file explorer and delete photo
	Given I select a photo using the file explorer or drag-n-drop
	And It's a photo of my face
	When I press delete before it finished checking for a face
	Then I should see a message saying that "the image has been deleted"

@RegisterWithPhotoAndDeletePhotos
Scenario: Register using file explorer and delete all photos
	Given I select 3 photos using the file explorer or drag-n-drop
	And They're photos of my face
	When I press delete
	Then I should the last image dropped

@ChangeProfilePictureSuccess
Scenario: Change profile picture
	Given I select a photo to be my current profile picture
	And It's a photo of my face
	When I press save 
	Then My profile picture should update and I should be taken back to the dashboard

@ChangeProfilePictureFail
Scenario: Change profile picture failure
	Given I select a photo to be my current profile picture
	And It is a photo of something that is not a face
	When I try to press save 
	Then The save button should be disabled