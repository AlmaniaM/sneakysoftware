﻿using System;
using NUnit.Framework;
using Elimination.Controllers;
using System.Web.Mvc;
using Elimination.Models;
using Elimination.Repositories;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;

namespace Test
{

    [TestFixture]
    class HM_moq_Tests
    { 
        [Test]
        public void Test_addBadge()
        {
            PlayerBadge pb = new PlayerBadge { PlayerID = 1, BadgeID = 2, DateEarned = DateTime.Today, GameID = 0 };
            var repoManager = new Mock<IRepoManager>();
            var bRepo = new Mock<IBadgeRepository>();

            bRepo.Setup(br => br.Badge(1, 2, 0)).Returns(true);
            repoManager.Setup(rm => rm.Badges).Returns(bRepo.Object);
           
            //function in game controller
            var mGame = new GamesController(repoManager.Object);
            var test = mGame.AddBadge(1, 2, 0);

           Assert.That(test, Is.EqualTo(true));
        }

        [Test]
        public void Test_addBadge_with_negative()
        {
            PlayerBadge pb = new PlayerBadge { PlayerID = 1, BadgeID = -3, DateEarned = DateTime.Today, GameID = 0 };
            var repoManager = new Mock<IRepoManager>();
            var bRepo = new Mock<IBadgeRepository>();

            bRepo.Setup(br => br.Badge(1, -3, 0)).Returns(false);
            repoManager.Setup(rm => rm.Badges).Returns(bRepo.Object);

            //function in game controller
            var mGame = new GamesController(repoManager.Object);
            var test = mGame.AddBadge(1, -3, 0);

            Assert.That(test, Is.EqualTo(false));
        }

        [Test]
            public void Test_That_badge_returns_true_if_Badge_does_not_exist_already()
            {
            //setup 
            var mockRepoManager = new Mock<IRepoManager>();
                var mockBadgeRepo = new Mock<IBadgeRepository>();
                mockRepoManager.Setup(x => x.Badges)
                              .Returns(mockBadgeRepo.Object);
                TestMoq sut = new TestMoq(mockRepoManager.Object);

                var expected = true;
                mockBadgeRepo.Setup(r => r.Badge(1, 2, 0)).Returns(expected);


                //try adding HostBadge
                var actual = sut._RepoManager.Badges.Badge(1, 2, 0);

                //Assert that actual = expected
                Assert.That(actual, Is.EqualTo(expected));
                mockBadgeRepo.VerifyAll();
            }


            [Test]
            public void Test_that_if_badge_exists_returns_false()
            {
                //setup 
                var mockRepoManager = new Mock<IRepoManager>();
                var mockBadgeRepo = new Mock<IBadgeRepository>();
                mockRepoManager.Setup(x => x.Badges)
                              .Returns(mockBadgeRepo.Object);
                TestMoq sut = new TestMoq(mockRepoManager.Object);

                var expected = false;
                mockBadgeRepo.Setup(r => r.Badge(1, 2, 0)).Returns(expected);

                //try adding HostBadge again
                var actual = sut._RepoManager.Badges.Badge(1, 2, 0);

                //Assert that actual = expected
                Assert.That(actual, Is.EqualTo(expected));
                mockBadgeRepo.VerifyAll();
            }

        }

    }


