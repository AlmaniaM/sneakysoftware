﻿using Gherkin.Ast;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using RestSharp;
using SimpleJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Helpers;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using Elimination.Controllers.Api;
using System.Web.Http;
using Elimination.Repositories;

namespace Test
{
    [Binding]
    public sealed class ID271_LoginAPISteps
    {
        dynamic data;
        string email;
        private RestClient client = new RestClient();
        private RestClient knownEmail = new RestClient();

        ///////////////////////////BACKGROUND-SETUP///////////////////////////////////

        [Given(@"the following table")]
        public void setTable(Table table)
        {
            data = table.CreateDynamicInstance();

        }

        [Given(@"get a player's email")]
        public void getAPlayer()
        {
            //get the email of first player
            knownEmail = new RestClient("http://localhost:1842/api/Players/GetPlayer?id=1");

        }

        ///////////////////////////////TEST-ONE///////////////////////////////////////

        [Given(@"I use API method GetPlayerInfo")]
        public void Given_I_Use_API_Method_GetPlayerInfo()
        {
            //get email 
            var request = new RestRequest(Method.GET);
            var response = knownEmail.Execute<JObject>(request);
            dynamic jsonObj = JsonConvert.DeserializeObject(response.Content);
            email = jsonObj.Email.ToString();
            //set client to getplayersInfo with that email
            client = new RestClient("http://localhost:1842/api/Players/GetPlayerInfo?email=" + email);
        }


        [Then(@"PlayerID should be returned as (.*)")]
        public void Then_PlayerID_Should_Be_ReturnedAs(int p0)
        {
            var request = new RestRequest(Method.GET);
            var response = client.Execute<JObject>(request);
            dynamic jsonObj = JsonConvert.DeserializeObject(response.Content);
            var PlayerID = jsonObj.PlayerID.ToString();

            //assert that you got player 1
            Assert.That(PlayerID.Equals("1"));
        }

        ///////////////////////////////TEST-TWO/////////////////////////////////////

        [Given(@"I use API method GetPlayerInfo without email")]
        public void WithoutEmail()
        {
            client = new RestClient("http://localhost:1842/api/Players/GetPlayerInfo?email=");
        }

        [Then(@"error message should be returned")]
        public void Error()
        {
            string ErrorMessage = "The request is invalid.";
            var request = new RestRequest(Method.GET);
            var response = client.Execute<JObject>(request);
            dynamic jsonObj = JsonConvert.DeserializeObject(response.Content);
            var error = jsonObj.Message.ToString();
            Assert.That(error.Equals(ErrorMessage));

        }

        //////////////////////////////TEST-THREE/////////////////////////////////////////

        [Given(@"I use API method GetPlayerInfo with wrong email")]
        public void WrongEmail()
        {
            client = new RestClient("http://localhost:1842/api/Players/GetPlayerInfo?email=notAnEmail@m.com");
        }

        [Then(@"I should see an error")]
        public void WrongEmailError()
        {
            string ErrorMessage = "An error has occurred.";
            var request = new RestRequest(Method.GET);
            var response = client.Execute<JObject>(request);
            dynamic jsonObj = JsonConvert.DeserializeObject(response.Content);
            var error = jsonObj.Message.ToString();
            Assert.That(error.Equals(ErrorMessage));
        }
    }
}

