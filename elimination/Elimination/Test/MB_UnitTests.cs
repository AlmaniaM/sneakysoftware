﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using Elimination.Controllers;
using Elimination.Models;
using Elimination.Repositories;
using Moq;
using NUnit.Framework;

namespace Test
{
    [TestFixture]
    public class MB_UnitTests
    {
        Mock<IRepoManager> _managerMock;
        private IEnumerable<Badge> _playerListOfBadges;
        private DashboardController _dashController;
        private PlayersController _playersController;

        private Player _player;
        private Badge _incrementalEliminatOneBage;
        private PlayerBadge _incrementalOnePlayerBadge;

        private string _authId = "af97221c-5c3e-4809-8fd6-bbc5c9733e1c";

        [SetUp]
        public void SetUpEnvironments()
        {
            _managerMock = new Mock<IRepoManager>();

            _incrementalEliminatOneBage = new Badge()
            {
                BadgeID = 7,
                BadgeName = "Eliminate-1-Badge",
                BadgeRequirement = 1,
                BadgeDescription = "You eliminated a player",
                Url = null
            };

            _playerListOfBadges = new List<Badge>()
            {
                _incrementalEliminatOneBage
            };

            _player = new Player()
            {
                PlayerID = 1,
                FirstName = "Michael",
                LastName = "Brown",
                UserName = "mgbrown08",
                Email = "mgbrown08@wou.com",
                Phone = "1234567890",
                DOB = DateTime.Parse("3/12/2018 12:00:00 AM"),
                PhotoUrl = "https://eliminationphoto.blob.core.windows.net/almania/alexander molodyh.jpeg",
                Profile = "alsdkjflaskd",
                AuthUserID = "af97221c-5c3e-4809-8fd6-bbc5c9733e1c"
            };

            _incrementalOnePlayerBadge = new PlayerBadge()
            {
                Badge = _incrementalEliminatOneBage,
                BadgeID = _incrementalEliminatOneBage.BadgeID,
                DateEarned = DateTime.Now.Date,
                GameID = -1,
                PlayerID = _player.PlayerID
            };

            _managerMock.Setup(mm => mm.Players.GetPlayerByAuthId(_authId)).Returns(_player);

            _managerMock.Setup(mm => mm.Badges.GetBadgesByPlayer(_player)).Returns(_playerListOfBadges);

            _playersController = new PlayersController(_managerMock.Object);

            _dashController = new DashboardController(_managerMock.Object);
        }

        [Test]
        public void Test_BadgesViewIsGenerated()
        {
            var playerBadges = _dashController.BadgesHelper(_authId) as ViewResult;

            Assert.IsNotNull(playerBadges);
            Assert.IsInstanceOf<ViewResult>(playerBadges);

        }

        [Test]
        public void Test_BadgesListIsGenerated()
        {
            var badgesList = _dashController.GenerateBadgeList(_player);

            Assert.AreEqual(_playerListOfBadges, badgesList);
        }
    }
}