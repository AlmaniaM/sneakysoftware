﻿Feature: APIGameControllerSteps
	As a developer 
	I want easier access to my players target information 
	so that playing the game would be easier.

@GetGamesEmptyDB
Scenario: GetGames returns error because of no games
	Given There are no games in the database
	When I call GetGames
	Then the result should be an error that says there are no games in the database

@GetGamesLiteEmptyDB
Scenario: GetGamesLite returns error because of no games
	Given There are no games in the database for GetGamesLite
	When I call GetGamesLite
	Then the result from GetGamesLite should be an error that says there are no games in the database

@GetGameWithGameId
Scenario: GetGame returns error because no game exists with gameID 20
	Given I set gameID to 20
	When I call GetGame with gameID
	Then the error from GetGameWithGameId should say GameID 20 does not exist in the database

@GetGameLiteWithGameId
Scenario: GetGameLite returns error because no game exists with gameID 20
	Given I set gameID in GetGameLite to 20
	When I call GetGameLite with gameID
	Then the error from GetGameLiteWithGameId should say GameID 20 does not exist in the database

@GetHostGamesByPlayerID
Scenario: GetHostGames returns error because host does not exist
	Given I set host PlayerID in GetHostGames to 100
	When I call GetHostGames with PlayerID
	Then the error from GetHostGamesByPlayerID should say There is no host with id of 100

@GetHostGamesByPlayerID2
Scenario: GetHostGames returns error because host does not have any games
	Given I set host PlayerID GetHostGamesTwo to 200
	When I call GetHostGames with PlayerID
	Then the error from GetHostGamesByPlayerIDTwo should say There are no games for the host with id of 200

@GetGameList 
Scenario: GetGameList retuns an error becuase there are no games with the ids of 1,20,40
	Given I set string ids to "1,20,40"
	When I call GetGameList with ids
	Then The error from GetGameList should say There are no games for these id's: "1,20,40"

@GetAllPlayersForGame
Scenario: GetAllPlayers returns an error because there are no game with id of 2
	Given I set gameId in GetAllPlayers to 2
	When I call GetAllPlayers with id
	Then the error from GetAllPlayersForGame should be There is no game with the GameID of 2

@JoinGameWithBadGameId
Scenario: JoinGame returns error because there is no game with gameId of 1
	Given I set gameId in JoinGameWithBadGameId to 1
	And I set playerId in JoinGameWithBadGameId to 200
	When I call JoinGame with gameId and playerId
	Then the error from JoinGameWithBadGameId should be There is no game with the GameID of: 1.

@JoinGameWithBadPlayerId
Scenario: JoinGame returns error because there is no player with PlayerId of 20000
	Given I set gameId in JoinGameWithBadPlayerId to 2000
	And I set playerId in JoinGameWithBadPlayerId to 20000
	When I call JoinGame with gameId and playerId
	Then the error from JoinGameWithBadPlayerId should be There is no player with PlayerID of: 20000.

@JoinGameWithBadPlayerIdAndGameId
Scenario: JoinGame returns error because there is no player with PlayerId of 300 and no game with GameID of 30
	Given I set gameId in JoinGameWithBadPlayerIdAndGameId to 30
	And I set playerId in JoinGameWithBadPlayerIdAndGameId to 300
	When I call JoinGame with gameId and playerId
	Then the error from JoinGameWithBadPlayerIdAndGameId should be Name "API JoinGame Call" and Error "There is no player with PlayerID of: 300 and no game with GameID of 30.";

@JoinGameWithExistingPlayerInGame
Scenario: JoinGame returns error because there is already a player with PlayerId of 400 and game with GameID of 2000
	Given I set gameId in JoinGameWithExistingPlayerInGame to 2000
	And I set playerId in JoinGameWithExistingPlayerInGame to 400
	And player is already in game
	When I call JoinGame with gameId and playerId and player is already in game
	Then the error from JoinGameWithExistingPlayerInGame should be Name "API JoinGame Call" and Error "Player with PlayerID of 400 has already joined this game.".

@JoinGameAsHost
Scenario: JoinGame returns error because a host can't join their own game
	Given I set gameId in JoinGameAsHost to 2000
	And I set playerId in JoinGameAsHost to 200
	And player is the host of the game
	When I call JoinGame with gameId and playerId as the host
	Then the error from JoinGameAsHost should be Name "API JoinGame Call" and Error "Player with id of 200 is the host. You cannot join your own game."