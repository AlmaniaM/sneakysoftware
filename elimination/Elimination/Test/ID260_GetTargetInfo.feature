﻿Feature: ID260_GetTargetInfo
	In order to easily use get my target information
	I want a simple method to retrieve it based on my PlyaerID

Scenario: I don't send a PlayerID
	Given I use API method GetTargetInfo without PlayerID
	Then error message should be recived


Scenario: I send a PlayerID that does not exist
	Given I use API method GetTargetInfo with a PlayerID that doesn't exist
	Then I should  recive an error 



Scenario: I send a PlayerID that is not an int
	Given I use API method GetTargetInfo with a PlayerID that is not an int
	Then I should get an error 