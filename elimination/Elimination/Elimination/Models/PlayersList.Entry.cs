﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Elimination.Models
{
    public partial class PlayersList
    {
        public void AddEntry(Player player, Game game, int targetCode)
        {
            Player = player;
            PlayerID = player.PlayerID;
            Game = game;
            GameID = game.GameID;
            TargetCode = targetCode;
        }

        public void RemovePlayerFromGame(Player player, Game game)
        {
            player.PlayersLists.Where(pl => pl.PlayerID == PlayerID).FirstOrDefault().Player = null;
            game.PlayersLists.Where(pl => pl.GameID == GameID).FirstOrDefault().Game = null;

            player.PlayersLists.Remove(this);
            game.PlayersLists.Remove(this);
        }
    }
}