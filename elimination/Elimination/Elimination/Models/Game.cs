namespace Elimination.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Game
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Game()
        {
            Attempts = new HashSet<Attempt>();
            Eliminations = new HashSet<Elimination>();
            PlayersLists = new HashSet<PlayersList>();
            Targets = new HashSet<Target>();
            Players = new HashSet<Player>();
        }

        public int GameID { get; set; }

        [Required]
        [StringLength(64)]
        public string Name { get; set; }

        public int Host { get; set; }

        public bool Active { get; set; }

        public bool? AccesLevel { get; set; }
        [Display(Name = "Start Date")]
        public DateTime StartDate { get; set; }
        [Display(Name = "End Date")]
        public DateTime? EndDate { get; set; }

        public int? Winner { get; set; }

        public Player WinnerObj { get; set; }

        [Required]
        [StringLength(64)]
        public string Location { get; set; }

        public decimal LocationCenterLong { get; set; }

        public decimal LocatoinCenterLat { get; set; }

        public int LocationRadius { get; set; }


        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Attempt> Attempts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Elimination> Eliminations { get; set; }

        public virtual Player Player { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PlayersList> PlayersLists { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Target> Targets { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Player> Players { get; set; }
    }

    public class EliminatePlayerViewModel
    {
        public int gameID { get; set; }
        public int playerID { get; set; }
        public int targetCodeInput { get; set; }
    }
}
