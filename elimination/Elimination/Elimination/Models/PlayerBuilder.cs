﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Elimination.Models
{
    public class PlayerBuilder
    {
        private PlayerBuilder(string[] user, ApplicationUser appUser)
        {
            if(user != null)
            {
                player = new Player
                {
                    FirstName = user[0]
                    , LastName = user[1]
                    , DOB = System.Convert.ToDateTime(user[2].ToString())
                    , UserName = user[3]
                    , Email = user[4]
                    , Phone = user[5]
                    , Profile = user[8]
                    , PhotoUrl = ""
                    , AuthUserID = appUser.Id
                };

                this.player = player;
            }
        }

        private Player player { get; set; }

        public static Player BuildPlayer(string[] user, ApplicationUser appUser)
        {
            PlayerBuilder playerBuilder = new PlayerBuilder(user, appUser);
            return playerBuilder.player;
        }
    }
}