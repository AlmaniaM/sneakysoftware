﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ninject.Infrastructure.Language;

namespace Elimination.Models
{
    public class JsonBadges
    {
        public string ReturnCode { get; set; }
        public string ReturnCodeDescription { get; set; }

        public List<JBadge> Badges { get; set; }

        public void AddBadges(List<Badge> badges)
        {
            badges.ForEach(b =>
            {
                var badge = new JBadge()
                {
                    BadgeID = b.BadgeID,
                    BadgeName = b.BadgeName,
                    BadgeRequirement = b.BadgeRequirement,
                    BadgeDescription = b.BadgeDescription,
                    Url = b.Url
                };

                Badges.Add(badge);
            });
        }
    }

    public class JBadge
    {
        public int BadgeID { get; set; }
        public string BadgeName { get; set; }
        public int? BadgeRequirement { get; set; }
        public string BadgeDescription { get; set; }
        public string Url { get; set; }
    }
}