﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Web;
using Microsoft.Ajax.Utilities;

namespace Elimination.Models.Api
{
    public class JsonBuilder
    {
        /// <summary>
        /// Builds a JsonGame from a Game model that include all the players in the game.
        /// </summary>
        /// <param name="game"></param>
        /// <returns></returns>
        public static JsonGame BuildGame(Game game)
        {
            var jsonGame = BuildGameLite(game);
            jsonGame.Players = BuildPlayers(game).ToList();
            return jsonGame;
        }

        /// <summary>
        /// Builds a JsonGame from a game model that doesn't contain the players information.
        /// </summary>
        /// <param name="game"></param>
        /// <returns></returns>
        public static JsonGame BuildGameLite(Game game)
        {
            return new JsonGame()
            {
                GameID = game.GameID,
                Name = game.Name,
                Host = game.Host,
                Active = game.Active,
                AccesLevel = game.AccesLevel,
                StartDate = game.StartDate,
                EndDate = game.EndDate,
                Location = game.Location,
                LocationCenterLong = game.LocationCenterLong,
                LocatoinCenterLat = game.LocatoinCenterLat,
                LocationRadius = game.LocationRadius
            };
        }

        //public static Game BuildGameFromJson(string jsonGame)
        //{

        //    Game game = null;
        //    try
        //    {
        //        JsonGame jGame = JsonConvert.DeserializeObject<JsonGame>(jsonGame);
        //        game = new Game
        //        {
        //            GameID = game.GameID,
        //            Name = game.Name,
        //            Host = game.Host,
        //            Active = game.Active,
        //            AccesLevel = game.AccesLevel,
        //            StartDate = game.StartDate,
        //            EndDate = game.EndDate,
        //            Location = game.Location,
        //            LocationCenterLong = game.LocationCenterLong,
        //            LocatoinCenterLat = game.LocatoinCenterLat,
        //            LocationRadius = game.LocationRadius,
        //            Players = jGame.Players.ToList()
        //        };
        //    }
        //    catch (Exception e) { }
        //}


        /// <summary>
        /// Builds a list of games with all players in it.
        /// </summary>
        /// <param name="games"></param>
        /// <returns></returns>
        public static IEnumerable<JsonGame> BuildGames(List<Game> games) => games.Select(BuildGame);


        /// <summary>
        /// Builds a list of JsonGames that don't include the list of players in it.
        /// </summary>
        /// <param name="games"></param>
        /// <returns></returns>
        public static IEnumerable<JsonGame> BuildGamesLite(IEnumerable<Game> games) => games.Select(BuildGameLite);

        /// <summary>
        /// Builds a JsonPlayer from a Player model.
        /// </summary>
        /// <param name="player"></param>
        /// <returns></returns>
        public static JsonPlayer BuildPlayer(Player player)
        {
            return new JsonPlayer
            {
                PlayerID = player.PlayerID,
                FirstName = player.FirstName,
                LastName = player.LastName,
                UserName = player.UserName,
                Email = player.Email,
                Phone = player.Phone,
                DOB = player.DOB,
                PhotoUrl = player.PhotoUrl,
                Profile = player.Profile,
                AuthUserID = player.AuthUserID
            };
        }

        /// <summary>
        /// Builds a list of JsonPlayers from a games PlayersList model.
        /// </summary>
        /// <param name="game"></param>
        /// <returns></returns>
        public static IEnumerable<JsonPlayer> BuildPlayers(Game game)
        {
            return game.PlayersLists.Select(playersList => BuildPlayer(playersList.Player)).ToList();
        }
    }
}