﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Elimination.Models
{
    public partial class Game
    {
        /// <summary>
        /// Starts the game and initializes player targets
        /// </summary>
        public void StartGame()
        {
            this.Active = true;

            Player[] players = Players.ToArray();
            Random r = new Random();
            
            // Randomize list
            for (int i = 0; i < players.Count(); i++)
            {
                int n = r.Next(0, players.Count());
                Player temp = players[i];
                players[i] = players[n];
                players[n] = temp;
            }

            // Add Targets to Game
            for (int i = 0; i < players.Count(); i++)
            {
                Target t = new Target
                {
                    Player = players[i].PlayerID,
                    TargetID = players[i+1 < players.Count() ? i+1 : 0].PlayerID,
                    GameID = GameID
                };
                Targets.Add(t);
            }
        }

        /// <summary>
        /// Removes the player from the game and removes 
        /// reassigns the targets
        /// </summary>
        /// <param name="playerID"></param>
        public void RemovePlayer(int playerID)
        {
            if (Active)
            {
                // Get the Target and the Attacker
                var predator = Targets.FirstOrDefault(t => t.TargetID == playerID);
                var prey = Targets.FirstOrDefault(t => t.Player == playerID);

                if (predator != null)
                {
                    // Remove both target objects
                    Targets.Remove(predator);
                    Targets.Remove(prey);

                    // If the predator and the prey are different
                    if (predator.Player != prey.TargetID)
                    {
                        Target t = new Target
                        {
                            Player = predator.Player,
                            TargetID = prey.TargetID,
                            GameID = this.GameID
                        };
                        Targets.Add(t);
                    }
                    else //If they are the same we have a win condition
                    {
                        Winner = predator.Player;
                        WinnerObj = predator.APlayer;
                        GameBadges.CheckGameBadges(Players.FirstOrDefault(p => p.PlayerID == predator.Player), this);

                        //Win condition
                    }
                }
            }
            
            Players.Remove(Players.FirstOrDefault(p => p.PlayerID == playerID));
            PlayersLists.Remove(PlayersLists.FirstOrDefault(p => p.PlayerID == playerID && p.GameID == GameID));
        }

        /// <summary>
        /// Elminates a Player will send a win condition if met
        /// Updates stats of both Players involved.
        /// </summary>
        /// <param name="playerID">The Victim</param>
        /// <returns></returns>
        public Elimination EliminatePlayer(int playerID)
        {
            // Get the targets and Player objects
            Target elminatedTarget = Targets.FirstOrDefault(t => t.TargetID == playerID);
            Target nextTarget = Targets.FirstOrDefault(t => t.Player == playerID);
            Player elminator = Players.FirstOrDefault(p => p.PlayerID == elminatedTarget.Player);
            Player elminated = Players.FirstOrDefault(p => p.PlayerID == playerID);

            // Construct the elimination
            Elimination elminination = new Elimination()
            {
                Eliminated = playerID,
                Eliminator = elminator.PlayerID,
                GameID = GameID,
                TimeStamp = DateTime.Now.Date
            };

            // Check for win condition
            if (nextTarget.TargetID == elminatedTarget.Player)
            {
                // TODO: add game win functionaility 
                Winner = elminatedTarget.Player;
                WinnerObj = elminatedTarget.APlayer;
            }
            else
            {
                // If not win condition set new target
                Target newTarget = new Target
                {
                    Player = elminator.PlayerID,
                    TargetID = nextTarget.TargetID,
                    GameID = GameID
                };
                Targets.Add(newTarget);
            }

            Targets.Remove(nextTarget);
            Targets.Remove(elminatedTarget);

            Eliminations.Add(elminination);

            GameBadges.CheckGameBadges(elminator, this);

            return elminination;
        }
    }
}