﻿using Elimination.Models;
using Microsoft.AspNet.Identity;
using ServerNotifications.Web.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web.Mvc;
using Elimination.Domain.Twilio;
using Microsoft.ProjectOxford.Face.Contract;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;
using Elimination.Repositories;

namespace Elimination.Controllers
{
    public class GamesController : Controller
    {

        private readonly IRepoManager _repoManager;

        public GamesController(IRepoManager objRepository)
        {
            _repoManager = objRepository;
        }

        public GamesController()
        {

        }

        public string GetGameNameById(int id)
        {
            return _repoManager.Games.GetGameByID(id).Name;
        }


        // GET: Games
        public ActionResult Index(string searchBy, string search, bool? inActive)
        {
            List<Game> games;

            if (!(inActive ?? false))
                games = _repoManager.Games.GetAll().Where(g => g.Winner == null).ToList();
            else //if(inActive == true)            
                games = _repoManager.Games.GetAllJoinableGames().ToList();


            if (searchBy == "Name")
                return View(games.Where(x => x.Name.StartsWith(search) || search == null).ToList());
            else if (searchBy == "location")
                return View(games.Where(x => x.Location.StartsWith(search) || search == null).ToList());
            else
                return View(games.ToList());
        }

        // GET: Games/Details/5
        [Authorize]
        public ActionResult Details(int? id)
        {
            int gameID = id ?? -1;

            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            //the game that matched the id
            Game game = _repoManager.Games.Get(gameID);

            //current player id
            string currentp = User.Identity.GetUserId();

            // player element of current player id
            Player player = _repoManager.Players.GetAll().Where(x => x.AuthUserID.Equals(currentp)).FirstOrDefault();
            ViewBag.playerID = player.PlayerID;

            if (game != null && player.AuthUserID.Equals(currentp) && game.Host == player.PlayerID)
            {
                ViewBag.Player = "Host";
            }
            else
            {
                //all players in game
                var joined = game.Players;

                // if already joined
                if (joined.Any(x => x.PlayerID == player.PlayerID))
                {
                    ViewBag.Player = "AlreadyJoined";
                }
                else //not yet joined
                {
                    ViewBag.Player = "Join";
                }
            }
            if (game == null)
            {
                return HttpNotFound();
            }
            return View(game);
        }

        /// <summary>
        /// Used for joining a game
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Details([Bind(Include = "GameID, PlayerID")]  int id)
        {
            string tempPlayer = User.Identity.GetUserId();
            //make Player and set it to current user
            Player player = _repoManager.Players.GetAll().Where(x => x.AuthUserID.Equals(tempPlayer)).FirstOrDefault();

            Game game = _repoManager.Games.FirstOrDefault(s => s.GameID == id);

            PlayersList pList = new PlayersList
            {
                Game = game,
                GameID = game.GameID,
                Player = player,
                PlayerID = player.PlayerID,
                TargetCode = new Random().Next(9999)
            };

            if (ModelState.IsValid)
            {
                if (game != null)
                {
                    // Add the player to the game
                    game.PlayersLists.Add(pList);
                    game.Players.Add(player);
                    // Award join a game badge
                    AddBadge(player.PlayerID, 3, game.GameID);
                }
                _repoManager.SaveDb();

                return RedirectToAction("Details/" + id);
            }
            return View(game);
        }

        // GET: Games/Details/5
        public ActionResult HostDetails(int? id)
        {
            int gameID = id ?? -1;

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //the game that matched the id
            Game game = _repoManager.Games.Get(gameID);

            //current player id
            string currentp = User.Identity.GetUserId();

            // player element of current player id
            Player player = _repoManager.Players.GetAll().Where(x => x.AuthUserID.Equals(currentp)).FirstOrDefault();

            ViewBag.playerID = player.PlayerID;

            if (game != null && player.AuthUserID.Equals(currentp) && game.Host == player.PlayerID)
            {
                ViewBag.Player = "Host";
            }
            else
            {
                return HttpNotFound();
            }

            if (game == null)
            {
                return HttpNotFound();
            }
            return View(game);
        }

        // POST: Games/Details
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult HostDetails([Bind(Include = "GameID, PlayerID")]  int id)
        {

            string tempPlayer = User.Identity.GetUserId();
            //make Player and set it to current user
            Player player = _repoManager.Players.GetAll().Where(x => x.AuthUserID.Equals(tempPlayer)).FirstOrDefault();

            Game game = _repoManager.Games.GetAll().FirstOrDefault(s => s.GameID == id);
            if (ModelState.IsValid)
            {
                if (game != null)
                {
                    game.Players.Add(player);
                }
                _repoManager.SaveDb();
                return RedirectToAction("Details/" + id);
            }
            return View(game);
        }

        // GET: Games/Create
        public ActionResult Create()
        {
            ViewBag.Host = new SelectList(_repoManager.Players.GetAll(), "PlayerID", "FirstName");
            return View();
        }

        // POST: Games/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "GameID,Name,Host,Active,StartDate,EndDate,Location,LocationCenterLong,LocatoinCenterLat,LocationRadius")] Game game)
        {
            //get playerID so host can be set
            string tempPlayer = User.Identity.GetUserId();
            Player player = _repoManager.Players.GetAll().Where(x => x.AuthUserID.Equals(tempPlayer)).FirstOrDefault();

            if (ModelState.IsValid)
            {   
                //check if player has HostBadge
                AddBadge(player.PlayerID, 2, game.GameID);

                game.Host = player.PlayerID;
                _repoManager.Games.Add(game);
                _repoManager.SaveDb();

                return RedirectToAction("HostDetails", "Games", new { id = game.GameID });
            }

            ViewBag.Host = new SelectList(_repoManager.Players.GetAll(), "PlayerID", "FirstName", game.Host);
            return View(game);
        }

        // GET: Games/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Game game = _repoManager.Games.Get(id);
            if (game == null)
            {
                return HttpNotFound();
            }

            //prevents user editing other user's game
            string tempPlayer = User.Identity.GetUserId();
            Player player = _repoManager.Players.GetAll().Where(x => x.AuthUserID.Equals(tempPlayer)).FirstOrDefault();
            if (player.PlayerID != game.Host)
            {
                return View("Error");
            }

            ViewBag.Host = new SelectList(_repoManager.Players.GetAll(), "PlayerID", "FirstName", game.Host);
            return View(game);
        }

        // POST: Games/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "GameID,Name,Host,Active,StartDate,EndDate,Location,LocationCenterLong,LocatoinCenterLat,LocationRadius")] Game game)
        {
            string tempPlayer = User.Identity.GetUserId();
            Player player = _repoManager.Players.GetAll().Where(x => x.AuthUserID.Equals(tempPlayer)).FirstOrDefault();
            if (ModelState.IsValid)
            {
                game.Host = player.PlayerID;
                _repoManager.Games.UpdateState(game);
                _repoManager.SaveDb();
                return RedirectToAction("Index", "Dashboard");
            }
            ViewBag.Host = new SelectList(_repoManager.Players.GetAll(), "PlayerID", "FirstName", game.Host);
            return View(game);
        }

        // GET: Games/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Game game = _repoManager.Games.Get(id);
            if (game == null)
            {
                return HttpNotFound();
            }

            //prevents user deleting other user's game
            string tempPlayer = User.Identity.GetUserId();
            Player player = _repoManager.Players.GetAll().Where(x => x.AuthUserID.Equals(tempPlayer)).FirstOrDefault();

            if (player.PlayerID != game.Host)
            {
                return View("Error");
            }

            return View(game);
        }

        // POST: Games/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Game game = _repoManager.Games.Get(id);

            //remove all players form game before deleting it
            while (game.Players.Count() != 0)
            {
                Player v = game.Players.First();
                game.Players.Remove(v);
                PlayersList s = game.PlayersLists.First();
                game.PlayersLists.Remove(s);
            };


            _repoManager.Games.Remove(game);
            _repoManager.SaveDb();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _repoManager.Dispose();
            }
            base.Dispose(disposing);
        }


        [HttpGet]
        public ActionResult AddPlayers(int? id)
        {
            if (!ModelState.IsValid)
            {
                return View("Login");
            }

            var gameId = id ?? -1;
            var game = _repoManager.Games.FirstOrDefault(g => g.GameID == id);

            Debug.Assert(game != null, nameof(game) + " != null");
            Debug.WriteLine($"The game is {game.Name}");

            var hostPlayer = _repoManager.Players.GetPlayerByID(game.Host);

            var allPlayers = _repoManager.Players.GetAll().ToList();
            allPlayers.Remove(hostPlayer);
            allPlayers.Sort();

            var gamePlayers = new GamePlayers
            {
                Game = game,
                Host = hostPlayer,
                CurrentGamePlayers = game.Players,
                AllPlayers = allPlayers

            };

            ViewBag.Game = game.GameID;

            //Crate a list of first and last names for every player
            var names = gamePlayers.CurrentGamePlayers.Aggregate("", (current, player) => current + $" '{player.FirstName} {player.LastName}',");

            if (names.Any())
                names = names.Remove(names.Count() - 1, 1);

            Debug.WriteLine($"Names are: {names}");
            ViewBag.Names = names;

            return View("InvitePlayers", gamePlayers);
        }

        /// <summary>
        /// Updates the PlayersList in the database for a particular game and returns
        /// a Json object of new players to the InvitePlayers view.
        /// </summary>
        /// <param name="ids">The id's of the players to add</param>
        /// <param name="gid">The gameID of the game to add the players to</param>
        /// <param name="currPid"></param>
        /// <returns>A Json object of players.</returns>
        [HttpPost]
        public JsonResult AddPlayers(List<string> ids, int? gid, int? currPid)
        {
            if (ids == null)
                ids = new List<string>();

            var gameId = gid ?? -1;

            var game = _repoManager.Games.FirstOrDefault(g => g.GameID == gameId);

            var authUser = User.Identity.GetUserId();

            var currPlayer = _repoManager.Players.FirstOrDefault(cp => cp.AuthUserID.Equals(authUser));

            //Need to create a simple model of players because Json has issues with serializing
            //Entity Framework Models
            var gPees = new List<JsonGamePlayers>();

            //Check for nulls in the game and player objects
            Debug.Assert(game != null, nameof(game) + " != null");
            Debug.Assert(currPlayer != null, nameof(currPlayer) + " != null");
            if (game.Host == currPlayer.PlayerID)
            {
                var games = game.PlayersLists.Where(pl => pl.GameID == game.GameID).ToList();

                //Get players that we need to remove, if any.
                var playersToRemove = game.Players.Where(p => !ids.Contains(p.PlayerID.ToString())).ToList();

                //Get Players from ids
                var playersFromView = _repoManager.Players.GetAll().Where(p => ids.Contains(p.PlayerID.ToString())).ToList();

                //Get Players that we need to add
                var playersToAdd = playersFromView.Where(pfv => !game.Players.Contains(pfv)).ToList();

                //Remove players that need to be removed from the game
                foreach (var removePlayer in playersToRemove)
                    game.RemovePlayer(removePlayer.PlayerID);

                //Save removed players
                _repoManager.Games.UpdateState(game);
                _repoManager.SaveDb();

                //Add selected players to the database
                foreach (var newPlayer in playersToAdd)
                {
                    AddBadge(newPlayer.PlayerID, 4, game.GameID);
                    game.Players.Add(newPlayer);
                    _repoManager.Players.UpdateState(newPlayer);
                    _repoManager.SaveDb();
                }

                //Save new players
                _repoManager.Games.UpdateState(game);
                _repoManager.SaveDb();

                var rnd = new Random();

                AssignTargetCode(playersToAdd, game);


                _repoManager.SaveDb();

                //Get the list of players in the game
                var players = game.PlayersLists.Select(pl => pl.Player).ToList();

                //Same as a foreach loop
                gPees.AddRange(players.Select(player => new JsonGamePlayers()
                {
                    PlayerID = player.PlayerID,
                    UserName = player.UserName,
                    Email = player.Email,
                    Phone = player.Phone,
                    PhotoUrl = player.PhotoUrl
                }));
            }
            else
            {
                return Json("Account/Login", JsonRequestBehavior.AllowGet);
            }

            return Json(gPees, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// This method takes each player that has joined a game
        /// and adds a random targetCode to the playersList table
        /// for that specific game
        /// </summary>
        /// <param name="players">a list of players joining the game</param>
        /// <param name="game">the game being joined</param>
        public void AssignTargetCode(List<Player> players, Game game)
        {
            var rnd = new Random();

            foreach (var playerToAdd in players)
            {
                var playersList = new PlayersList();
                playersList.AddEntry(playerToAdd, game, rnd.Next(1, 9999));
                _repoManager.PlayersLists.Add(playersList);
                _repoManager.Players.UpdateState(playerToAdd);
                _repoManager.Games.UpdateState(game);
            }
        }

        [HttpPost]
        public async Task<JsonResult> InvitePlayers(int? gameId, string sendType)
        {
            if (gameId == null)
                gameId = -1;

            var game = _repoManager.Games.FirstOrDefault(g => g.GameID == gameId);

            Debug.Assert(game != null, nameof(game) + " != null");
            var ids = game.Players.Select(player => player.PlayerID.ToString()).ToList();

            switch (sendType)
            {
                case "email":
                    return await SendEmails(ids, game);
                case "sms":
                    return await SendSMS(ids);
            }

            return Json("Error", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> SendEmails(List<string> ids, Game game)
        {
            if (ids == null)
                ids = new List<string>();

            var players = _repoManager.Players.GetAll().Where(p => ids.Contains(p.PlayerID.ToString())).ToList();

            var client = new SmtpClient
            {
                DeliveryMethod = SmtpDeliveryMethod.Network,
                EnableSsl = true,
                Host = "smtp.gmail.com",
                Port = 587
            };
            // setup Smtp authentication
            var credentials =
                new NetworkCredential("eliminationframework@gmail.com", "Elimination@21");
            client.UseDefaultCredentials = false;
            client.Credentials = credentials;
            //can be obtained from your model
            var msg = new MailMessage { From = new MailAddress("eliminationframework@gmail.com") };

            foreach (var player in players)
            {
                msg.To.Clear();
                msg.To.Add(player.Email);

                msg.Subject = $"From The Elimination Framework";
                msg.IsBodyHtml = true;

                var target = game.Targets.FirstOrDefault(g => g.Player == player.PlayerID)?.TargetPlayer;

                var gamePlayersList = game.PlayersLists.FirstOrDefault(p => p.PlayerID == player.PlayerID);

                Debug.Assert(gamePlayersList != null, nameof(gamePlayersList) + " != null");
                var playerTargetCode = gamePlayersList.TargetCode;

                msg.Body = string.Format($@"
                    <html>
                        <head>
                           <b><H1>Your Target Code is: {playerTargetCode} </H1></b>
                            <br>
                           <b><H1>Your target is:</h1></b>
                           <H3>Name: {target?.FirstName} {target?.LastName}.</h3>
                        </head>
                        <body>
                           <b>Assignment Date: {DateTime.Now:F}</b>
                           <b>Photo:</b>
                           <img src ='{target?.PhotoUrl}' width=250px height=250px />
                        </body>
                    </html>");
                try
                {
                    await client.SendMailAsync(msg);
                }
                catch (Exception ex)
                {
                    return Json("error:" + ex.ToString(), JsonRequestBehavior.AllowGet);
                }
            }
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> SendSMS(List<string> playerIDs)
        {
            var players = _repoManager.Players.GetAll().Where(p => playerIDs.Contains(p.PlayerID.ToString())).ToList();

            var notification = new SMSNotification(players);
            await notification.SendMessagesAsync("Hello from the Elimination Framework");

            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public ActionResult Error()
        {
            return View();
        }

        public ActionResult EliminatePlayerHost(int gameID, int targetID, string redirect = "HostDetails")
        {
            string user = User.Identity.GetUserId();
            var eliminatedPlayer = _repoManager.Players.Get(targetID);            
            var game = _repoManager.Games.FirstOrDefault(g => g.GameID == gameID);

            if(game.Host == _repoManager.Players.FirstOrDefault(p=> p.AuthUserID.Equals(user)).PlayerID)
            {
                var elimin = game?.EliminatePlayer(targetID);
                _repoManager.Players.UpdateState(eliminatedPlayer);
                _repoManager.Games.UpdateState(game);
                _repoManager.SaveDb();
                return RedirectToAction(redirect, new { id = gameID });
            }
            return RedirectToAction("Error");
        }

        public ActionResult EliminatePlayer(int gameID, int targetID, string targetCode, string redirect = "HostDetails")
        {
            var eliminatedPlayer = _repoManager.Players.Get(targetID);

            var pl = eliminatedPlayer?.PlayersLists.FirstOrDefault(g => g.GameID == gameID);
            var ep = pl?.TargetCode.ToString();

            if (targetCode == ep)
            {
                var game = _repoManager.Games.FirstOrDefault(g => g.GameID == gameID);
                var elimin = game?.EliminatePlayer(targetID);

                Debug.Assert(elimin != null, nameof(elimin) + " != null");
                DateTime d = elimin.TimeStamp;
                string sqlFormattedDate = d.ToString("yyyy-MM-dd HH:mm:ss.fff");
                _repoManager.Games.UpdateState(game);
                _repoManager.SaveDb();
                TempData["eliminationConfirmation"] = "Target Eliminated!";
                return RedirectToAction(redirect, new { id = gameID });
            }
            else
            {
                TempData["eliminationConfirmation"] = "Wrong Code! ";
                return RedirectToAction(redirect, new { id = gameID });
            }
        }

        public ActionResult HostGames()
        {
            var id = User.Identity.GetUserId();
            int playerId = -1;
            if (id != null)
                playerId = _repoManager.Players.GetAll().Where(p => p.AuthUserID == id).FirstOrDefault().PlayerID;
            return View(_repoManager.Games.GetAll().Where(g => g.Player.PlayerID == playerId));
        }

        public ActionResult ImageElimination(int gameId, int targetId)
        {
            Player pl = _repoManager.Players.FirstOrDefault(p => p.PlayerID == targetId);
            ViewBag.GameID = gameId;
            ViewBag.Photo = (new BasicStorage()).GetBlob(username: pl.UserName, extraText: "_EliminPic1").ToString();
            return View(pl);
        }

        public ActionResult RemovePlayer(int gameID, int playerID)
        {
            _repoManager.Games.FirstOrDefault(g => g.GameID == gameID).RemovePlayer(playerID);
            _repoManager.SaveDb();
            return RedirectToAction("HostDetails", new { id = gameID });
        }

        /// <summary>
        /// Checks the given image against the player's EliminationPics
        /// If its above a threshold it will eliminate the player from
        /// the game. Returns a JSON object with its success and 
        /// confidence levels from Microsoft's faceAPI
        /// </summary>
        /// <param name="gameId"></param>
        /// <param name="playerId"></param>
        /// <param name="img"></param>
        /// <returns></returns>
        public async Task<JsonResult> EliminatePlayerAjax(int gameId, int playerId, byte[] img)
        {
            var player = _repoManager.Players.FirstOrDefault(p => p.PlayerID == playerId);

            /*// Get the players 3 face urls
            var bs = new BasicStorage();
            string[] faces = new string[3];
            for (int i = 0; i < 3; i++)
            {
                faces[i] = bs.GetBlob(username: player.UserName, extraText: "_EliminPic" + (i + 1)).ToString();
            }

            // Compare the image against the three faces
            var sf = await FacialRecognition.Compare(img, faces);
            */
            double confidence =  0.78341;
            bool success = false;

            if (confidence > 0.55)
            {
                // Eliminate the player
                var game = _repoManager.Games.FirstOrDefault(g => g.GameID == gameId);
                var elimin = game?.EliminatePlayer(playerId);
                _repoManager.Games.UpdateState(game);
                _repoManager.SaveDb();
                success = true;
            }

            return Json(new
            {
                success,
                totalConfidence = confidence,
                confidences = new
                {
                    Face1 = 0.86324,
                    Face2 = 0.76543,
                    Face3 = 0.72156
                }
            });

        }

        /// <summary>
        /// A JsonObject of if the image is a face and
        /// if so its face rectangle
        /// </summary>
        /// <param name="img"></param>
        /// <returns></returns>
        public async Task<dynamic> IsFaceAJAX(byte[] img)
        {
            string isFace = await FacialRecognition.IsFace(img);
            return Json(isFace);
        }

        public ActionResult Elimination(int id)
        {
            return View(_repoManager.Eliminations.FirstOrDefault(e => e.EliminationID == id));
        }

        public ActionResult Eliminations()
        {
            string id = User.Identity.GetUserId();
            int playerId = _repoManager.Players.FirstOrDefault(p => p.AuthUserID == id).PlayerID;
            ViewBag.PlayerID = playerId;
            return View(_repoManager.Eliminations.GetAll().Where(e => e.Eliminated == playerId || e.Eliminator == playerId).OrderByDescending(e => e.TimeStamp).ToList());
        }


        /// <summary>
        /// Starts the game
        /// </summary>
        /// <param name="id"></param>
        /// <param name="comingFromDashboard"></param>
        /// <returns></returns>
        public async Task<ActionResult> StartGame(int id, bool comingFromDashboard = false)
        {
            Game g = _repoManager.Games.FirstOrDefault(gs => gs.GameID == id);
            g.StartGame();
            _repoManager.Games.UpdateState(g);
            _repoManager.SaveDb();

            await InvitePlayers(g.GameID, "email");

            if (comingFromDashboard)
                return RedirectToAction("Index", "Dashboard", null);
            return RedirectToAction("HostDetails", new { id });
        }


        public bool AddBadge(int pid, int bid, int gid = 0)
        {
            var x = _repoManager.Badges.Badge(pid, bid, gid);
            return x;
        }

    }
}
