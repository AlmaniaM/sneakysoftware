﻿using Elimination.Models;
using Microsoft.ProjectOxford.Face;
using Microsoft.ProjectOxford.Face.Contract;
using Newtonsoft.Json;
using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace Elimination.Controllers
{
    public class FacialRecognition
    {
        private static string apikey = WebConfigurationManager.AppSettings["FaceApi"];
        private static string apiroot = "https://westus.api.cognitive.microsoft.com/face/v1.0";

        /// <summary>
        /// Uploads the url to microsofts face api
        /// Returns a face object
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static async Task<Face> Upload(string url, bool returnFaceLandmarks = false)
        {
            try
            {
                var faceServiceClient = new FaceServiceClient(apikey, apiroot);
                //Make call to face api
                Face[] faces = await faceServiceClient.DetectAsync(url, returnFaceId: true, returnFaceLandmarks: returnFaceLandmarks);
                return faces[0];
            }
            catch (Exception e)
            {
                return new Face();
            }
        }

        /// <summary>
        /// Uploads the byte[] image to Microsofts face api
        /// Returns a face object
        /// </summary>
        /// <param name="img"></param>
        /// <returns></returns>
        public static async Task<Face> Upload(byte[] img, bool returnFaceLandmarks = false)
        {
            // Little conversoin trick to make sure img is the right type
            Image imgObj = Models.ImageConverter.byteArrayToImage(img);
            var imgArray = Models.ImageConverter.imageToByteArray(imgObj);

            try
            {
                var faceServiceClient = new FaceServiceClient(apikey, apiroot);
                Stream stm = new MemoryStream(imgArray);
                Face[] faces = await faceServiceClient.DetectAsync(stm, returnFaceId: true, returnFaceLandmarks: returnFaceLandmarks);
                if (faces.Length > 0)
                    return faces[0];
                else
                    return new Face();
            }
            catch (Exception e)
            {
                return new Face();
            }
        }

        /// <summary>
        /// Compares two faces to determine if they are similar
        /// One is a byte[] and one is a url. 
        /// In this project the byte[] is usually the attempt at elimination photo
        /// and the url is one of the profile pictures
        /// </summary>
        /// <param name="img"></param>
        /// <param name="url"></param>
        /// <returns></returns>
        public static async Task<SimilarFace> Compare(byte[] img, string url)
        {
            Face faceImg = await Upload(img);
            Face faceUrl = await Upload(url);

            try
            {
                if (faceImg.FaceId != null && faceUrl.FaceId != null)
                {
                    var faceServiceClient = new FaceServiceClient(apikey, apiroot);
                    SimilarFace[] sfs = await faceServiceClient.FindSimilarAsync(faceImg.FaceId, new Guid[] { faceUrl.FaceId });
                    return sfs[0];
                }
                else
                {
                    return new SimilarFace();
                }
            }
            catch (Exception e)
            {
                return new SimilarFace();
            }
        }

        /// <summary>
        /// Compares multiple url images to the byte[] image
        /// In this project only 3 faces are ever used so the method
        /// will only handle 3 (no less, no more)
        /// </summary>
        /// <param name="img"></param>
        /// <param name="urls"></param>
        /// <returns></returns>
        public static async Task<SimilarFace[]> Compare(byte[] img, string[] urls)
        {
            Face faceImg = await Upload(img);
            Face[] faces = new Face[] {
                await Upload(urls[0]),
                await Upload(urls[1]),
                await Upload(urls[2])
            };

            try
            {
                if (faceImg.FaceId != null && faces[0] != null && faces[1] != null && faces[2] != null)
                {
                    var faceServiceClient = new FaceServiceClient(apikey, apiroot);
                    SimilarFace[] sfs = await faceServiceClient.FindSimilarAsync(faceImg.FaceId, 
                        new Guid[] { faces[0].FaceId, faces[1].FaceId, faces[2].FaceId });
                    return sfs;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Determines if the given image is a face
        /// Returns a JsonString of a boolean and a FaceRectangle
        /// </summary>
        /// <param name="img"></param>
        /// <returns></returns>
        public static async Task<string> IsFace(byte[] img)
        {
            Face f = await Upload(img);
            if (f.FaceRectangle == null)
                return JsonConvert.SerializeObject(new { isFace = false });
            return JsonConvert.SerializeObject(new { isFace = true, Rectangle = f.FaceRectangle });
        }

        /// <summary>
        /// Determines if the given image is a face
        /// Returns a boolean
        /// </summary>
        /// <param name="img"></param>
        /// <returns></returns>
        public static async Task<bool> IsFaceBool(byte[] img)
        {
            Face f = await Upload(img);
            if (f.FaceRectangle == null)
                return false;
            return true;
        }
    }
}