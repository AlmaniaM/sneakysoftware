﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;
using Elimination.Models;
using Elimination.Repositories;
using Newtonsoft.Json;

namespace Elimination.Controllers.Api
{
    public class EliminationsController : ApiController
    {
        private readonly IRepoManager _repoManager;

        public EliminationsController(IRepoManager objRepository)
        {
            _repoManager = objRepository;
        }

        public EliminationsController()
        {

        }

        /// <summary>
        /// Gets all eliminations
        /// </summary>
        /// <returns></returns>
        public IQueryable<Models.Elimination> GetEliminations()
        {
            return _repoManager.Eliminations.GetAll().AsQueryable();
        }

        /// <summary>
        /// Gets an elimination with id equal to id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [ResponseType(typeof(Models.Elimination))]
        public IHttpActionResult GetElimination(int id)
        {
            Models.Elimination elimination = _repoManager.Eliminations.FirstOrDefault(e => e.EliminationID == id);
            if (elimination == null)
            {
                return NotFound();
            }

            return Json(new Models.Elimination()
            {
                EliminationID = elimination.EliminationID,
                Eliminated = elimination.Eliminated,
                Eliminator = elimination.Eliminator,
                GameID = elimination.GameID,
                TimeStamp = elimination.TimeStamp
            });
        }

        /// <summary>
        /// Posts an elimination to be varified and eliminated if it passes the photo recognition 
        /// and a valid targetCode submission.
        /// </summary>
        /// <param name="elimination">Is a minimal version of the <seealso cref="Elimination.Models.Elimination"/> class 
        /// that is used to register an elimination.</param>
        /// <returns>The next target if successful, otherwise an error confirmation will be returned.</returns>
        [ResponseType(typeof(EliminationConfirmation))]
        public async Task<IHttpActionResult> PostElimination(JsonElimination elimination)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //This is where all the magic happens
            EliminationManager manager = new EliminationManager(elimination, _repoManager);
            var playerEliminated = await manager.EliminatePlayer();

            return Json(playerEliminated);
        }

        /// <summary>
        /// Eliminate a target by target code.
        /// If <paramref name="targetCode"/> matches <paramref name="targetId"/>'s code then 
        /// they will be eliminated from the game
        /// </summary>
        /// <param name="gameId"></param>
        /// <param name="targetId">Player to be eliminated</param>
        /// <param name="targetCode">Code to be checked</param>
        /// <returns>
        /// Json(new {
        ///   Success = true if successful elimination,
        ///   NextTargetId = next target id if it was a success, -1 if the elimination won the game,
        ///   WonGame = true if elimination won the game
        /// })
        /// </returns>
        public async Task<IHttpActionResult> EliminateTargetViaCode(int gameId, int targetId, int targetCode)
        {
            Game g = _repoManager.Games.GetGameByID(gameId);
            PlayersList pl = g.PlayersLists.FirstOrDefault(p => p.PlayerID == targetId);
            int playerId = g.Targets.FirstOrDefault(t => t.TargetID == targetId).Player;

            if(pl.TargetCode == targetCode) // Successful Elimination
            {
                g.EliminatePlayer(targetId);
                _repoManager.Games.UpdateState(g);
                await _repoManager.SaveDbAsync();

                if(g.Winner != null)
                {
                    return Json(new
                    {
                        Success = true,
                        NextTargetId = -1,
                        WonGame = true
                    });
                }

                return Json(new {
                    Success = true,
                    NextTargetId = g.Targets.FirstOrDefault(t => t.Player == playerId).TargetID,
                    WonGame = false
                });
            }

            return Json(new { Success = false, NextTargetId = targetId, WonGame = false });
        }

        /// <summary>
        /// Struct for EliminatePlayerImage method to allow model-binding
        /// so parameters can be put in the body of thew request
        /// </summary>
        public struct EliminImg
        {
            public int gameId;
            public int playerId;
            public byte[] img;
        }

        /// <summary>
        /// Eliminate's your target if the photo matchs theirs
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A json with success, and confidence of the comparison</returns>
        [System.Web.Http.HttpPost]
        public async Task<JsonResult> EliminatePlayerViaImage(EliminImg data)
        {
            return await (new Controllers.GamesController(_repoManager)).EliminatePlayerAjax(data.gameId, data.playerId, data.img);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _repoManager.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}