﻿using Elimination.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using System.Drawing;
using System;
using System.Web.Mvc;
using System.Configuration;
using System.Net.Http;
using Newtonsoft.Json;
using Twilio.Jwt.AccessToken;
using Microsoft.Owin.Security;
using System.Security.Claims;
using System.Net;
using Microsoft.WindowsAzure.Storage.Shared.Protocol;
using System.Text;
using System.IO;
using System.Linq;
using System.Security;
using System.Text.RegularExpressions;
using System.Web.Helpers;
using Elimination.Repositories;
using Microsoft.Ajax.Utilities;

namespace Elimination.Controllers.Api
{
    public class AccountController : ApiController
    {
        private readonly IRepoManager _repoManager;
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public AccountController(){}

        public AccountController(IRepoManager objRepository)
        {
            _repoManager = objRepository;
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get => _signInManager ?? HttpContext.Current.GetOwinContext().Get<ApplicationSignInManager>();
            private set => _signInManager = value;
        }

        public ApplicationUserManager UserManager
        {
            get => _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            private set => _userManager = value;
        }

       
        /// <summary>
        /// Api method to register a new player
        /// Creates an asp.net user and a player
        /// Uses a default profile image if one is not supplied 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        [ResponseType(typeof(Player))]
        public async Task<IHttpActionResult> Register(RegisterViewModel model)
        {
            var user = new ApplicationUser {UserName = model.Email, Email = model.Email};
            var result = await UserManager.CreateAsync(user, model.Password);

            //If user didn't provide photo for registration then they get a default photo hosted on the server.
            if (model.Photo == null)
            {
                var img = Image.FromFile(System.Web.Hosting.HostingEnvironment.MapPath("~\\Images\\ninja-red-white.png")
                                         ?? throw new InvalidOperationException("Default Image On Server Not Found!!"));
                model.Photo = Models.ImageConverter.imageToByteArray(img);
            }

            Uri photoUri;

            try
            {
                //Get the profile photo url
                photoUri = await BasicStorage.SaveImageToBlobAsync(model.UserName, model.PhotoExtension, model.Photo);
            }
            catch (Exception e)
            {
                var error = new GeneralResponse()
                {
                    Name = "Call to Register failed at blob storage call.",
                    Error = $"Error is: {e.Message}."
                };
                return Json(error);
            }

            //Prints any errors that occured while trying to register the actual user model. 
            if (!result.Succeeded)
            {
                var listOfErrors = new List<RegisterUserResponse>();
                result.Errors.ForEach(e => listOfErrors.Add(new RegisterUserResponse(){Error = e}));
                return Json(listOfErrors);
            }

            await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);

            var player = new Player
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                DOB = model.DOB,
                UserName = model.UserName,
                Email = model.Email,
                Phone = model.Phone,
                PhotoUrl = photoUri.ToString(),
                Profile = model.Profile,
                AuthUserID = user.Id
            };

            _repoManager.Players.Add(player);
            await _repoManager.SaveDbAsync();

            return CreatedAtRoute("DefaultApi", new { id = player.PlayerID }, player);
        }
    }
}