﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Results;
using Elimination.Models;
using Elimination.Models.Api;
using Elimination.Repositories;
using Newtonsoft.Json;

namespace Elimination.Controllers.Api
{
    public class PlayersController : ApiController
    {
       
        private readonly IRepoManager _repoManager;

        public PlayersController(IRepoManager objRepository)
        {
           _repoManager = objRepository;
        }

        public PlayersController()
        {
        }

       
        /// <summary>
        /// Returns the player with the matching email
        /// Without nagivation properties
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public async Task<IHttpActionResult> GetPlayerInfo(string email)
        {
            int id = _repoManager.Players.GetPlayerIDyByEmail(email);
          
            Player rplayer = await _repoManager.Players.awaitGetPlayerByID(id);
            JsonPlayer jPlayer = JsonBuilder.BuildPlayer(rplayer);

            if (rplayer == null || jPlayer == null)
            {
                return NotFound();
            }
            return Ok(jPlayer);
        }

        /// <summary>
        /// Returns the player with the matching id
        /// Without nagivation properties
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [ResponseType(typeof(Player))]
        public async Task<IHttpActionResult> GetPlayer(int id)
        {
            Player player = await _repoManager.Players.awaitGetPlayerByID(id);

            //Need this custom Player model because Json makes a fuss about converting 
            //an EF Model to a Json object because of the self referencing Player lists inside
            //the Player models.
            JsonPlayer jPlayer = JsonBuilder.BuildPlayer(player);

            if (player == null || jPlayer == null)
            {
                return NotFound();
            }

            return Ok(jPlayer);
        }

        /// <summary>
        /// Updates the player with id to player
        /// </summary>
        /// <param name="id"></param>
        /// <param name="player"></param>
        /// <returns></returns>
        [ResponseType(typeof(HttpStatusCode))]
        public async Task<IHttpActionResult> UpdatePlayer(int id, Player player)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != player.PlayerID)
            {
                return BadRequest();
            }

            _repoManager.Players.UpdateState(player);

            try
            {
                await _repoManager.SaveDbAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PlayerExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return StatusCode(HttpStatusCode.OK);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
               _repoManager.Dispose();
            }
            base.Dispose(disposing);
        }

        /// <summary>
        /// Returns whether a player exists
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private bool PlayerExists(int id)
        {
            return _repoManager.Players.GetAll().Count(e => e.PlayerID == id) > 0;
        }

        // Struct used in the following method
        // Used for model binding so the contents 
        // can be sent in the body
        public struct ThreeImgs
        {
            public byte[] img1;
            public byte[] img2;
            public byte[] img3;
            public int playerId;
        }

        /// <summary>
        /// Uploads the three images to the player
        /// Images are required to be a face and will
        /// fail if they are not
        /// </summary>
        /// <param name="data"></param>
        /// <returns>
        /// A json string containing error imformation. 
        /// If string contains three true bools then the 
        /// everything suceed and the images should be 
        /// saved.
        /// </returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        public async Task<JsonResult<string>> UploadThreeFaces(ThreeImgs data)
        {
            Player player = _repoManager.Players.GetPlayerByID(data.playerId);
            string username = player.UserName;

            bool face1 = await FacialRecognition.IsFaceBool(data.img1);
            bool face2 = await FacialRecognition.IsFaceBool(data.img1);
            bool face3 = await FacialRecognition.IsFaceBool(data.img1);

            if(face1 && face2 && face3)
            {
                try
                {
                    var photoUri1 = await BasicStorage.SaveImageToBlobAsync2(username, ".png", data.img1, "_EliminPic1");
                }
                catch (Exception e)
                {
                    return Json(JsonConvert.SerializeObject(new
                    {
                        Error = e.Message,
                        StackTrace = e.StackTrace,
                        Message = "An error occured saveing Image 1 to the blob"
                    }));
                }

                try
                {
                    var photoUri2 = await BasicStorage.SaveImageToBlobAsync2(username, ".png", data.img2, "_EliminPic2");
                }
                catch (Exception e)
                {
                    return Json(JsonConvert.SerializeObject(new
                    {
                        Error = e.Message,
                        StackTrace = e.StackTrace,
                        Message = "An error occured saveing Image 2 to the blob"
                    }));
                }

                try
                {
                    var photoUri3 = await BasicStorage.SaveImageToBlobAsync2(username, ".png", data.img3, "_EliminPic3");
                }
                catch (Exception e)
                {
                    return Json(JsonConvert.SerializeObject(new
                    {
                        Error = e.Message,
                        StackTrace = e.StackTrace,
                        Message = "An error occured saveing Image 3 to the blob"
                    }));
                }
            }

            return Json(JsonConvert.SerializeObject(new {
                Img1IsFace = face1,
                Img2IsFace = face2,
                Img3IsFace = face3
            }));
        }
    }
}