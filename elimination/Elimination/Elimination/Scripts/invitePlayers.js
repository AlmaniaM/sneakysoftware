﻿
/**
 * This function is made specifically for the invitePlayers dropdown list in the invitePlayers view.
 * it goes through and checks which lists have been set to selected and checks the checkboxes and 
 * highlights the rows as well.
 */
function enableSelectedPlayers() {
    var options = $("#pick-players-dropdown option");
    var tobeSelectedLis = $("#pick-players-dropdown_child ul li");

    /*go through each row and set the highlight as well as check the checkbox*/
    for (var i = 0; i < $("#pick-players-dropdown option").length; i++) {

        //if the item has a class of selected, then set the coresponding 
        //li input to selected as well. When I say coresponding list, I mean the 
        //second ul because two lists are made when the dropdown is initially rendered.
        if (options[i].getAttribute("selected") === "selected") {
            tobeSelectedLis[i].classList.add("selected");
            tobeSelectedLis[i].firstChild.checked = true;
        }
    }
}

/**
 * Finds the currently selected players and returns a string of playerID's separated by commas 
 */
function getSelectedPlayers() {
    var selectedLis = $("#pick-players-dropdown_child ul li");
    var selectedPlayers = [];

    for (var i = 0; i < selectedLis.length; i++) {
        if (selectedLis[i].firstChild.checked === true) {
            selectedPlayers.push(selectedLis[i].firstChild.value);
        }
    }

    return selectedPlayers;
}