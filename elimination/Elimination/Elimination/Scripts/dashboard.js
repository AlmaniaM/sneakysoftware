﻿function checkPlayerEliminationCount(playerId) {
    var urlAddress = "Players/CheckForNewEliminationBadges";

    // Ajax call for checking for new eliminatoin badges
    // Done this way to show notifcations
    $.ajax({
        type: "POST",
        datatype: "JSON",
        url: urlAddress,
        data: { playerId: playerId },
        success: function (response) {
            var responseCode = response.ReturnCode;
            var badgeCount = response.Badges.length;
            var badges = response.Badges;

            for (var i = 0; i < badgeCount; i++) {

                $.notify({
                    icon: badges[i].Url,
                    title: 'You Earned A Badge',
                    message: badges[i].BadgeDescription
                }, {
                        type: 'minimalist',
                        icon_type: 'image',
                        template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
                            '<img data-notify="icon" class="pull-left">' +
                            '<span data-notify="title">{1}</span>' +
                            '<span data-notify="message">{2}</span>' +
                            '</div>'
                    });
            }
        },
        error: function (response) {
            console.log("In error function with response of: " + response[0]);
        }
    });
}

$(document).ready(function () {
    var playerId = $("#dashboard-playerId").val();

    // Setup the manual elimination button
    $('#manual-elimination-buton').on('click',
        function () {
            checkPlayerEliminationCount(playerId);
        });

    checkPlayerEliminationCount(playerId);
});