﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Elimination.Models;

namespace Elimination.Repositories.IRepositories
{
    public interface IAspNetRepository : IRepository<AspNetUser>
    {
        AspNetUser GetAspNetUserByEmail(string email);
        string GetAspNetUserIdByEmail(string email);
        List<string> GetAspNetUserIdsByEmail(string email);
    }
}
