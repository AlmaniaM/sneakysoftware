﻿using Elimination.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Ajax.Utilities;
using Microsoft.WindowsAzure.Storage.Table;

namespace Elimination.Repositories
{
    public interface IPlayerRepository : IRepository<Player>
    {
        IEnumerable<int> GetGameID(int pID, int tID);
        IEnumerable<int> GetPlayersTarget(int p);
        int GetPlayerIDyByEmail(String email);
        Task<Player> awaitGetPlayerByID(int p);
        IEnumerable<Player> GetPlayers();
        Player GetPlayerByAuthId(string id);
        Player GetPlayerByID(int p);
        Task<Player> GetPlayerByIdAsync(int p);
        IEnumerable<Player> GetPlayersFriends(Player p);
        IEnumerable<Player> GetPlayersNonFriends(Player p);
        Models.Elimination GetMostRecentElimination(Player p);
        Player GetMostRecentPlayersEliminatedBy(Player p);
        Player GetMostRecentEliminatorOf(Player p);
        IEnumerable<Player> GetTargetsEliminatedBy(Player p);
        IEnumerable<Player> GetPlayersWhoEliminated(Player p);
        int HowManyTimesHasPlayerBeenEliminated(Player player);
        int HowManyTargetsHasPlayerEliminated(Player player);
        int HowManyTimesHasPlayerEliminatedTarget(Player player, Player target);
        int HowManyTimesHasPlayerEliminatedSameTarget(Player player);
    }
}
