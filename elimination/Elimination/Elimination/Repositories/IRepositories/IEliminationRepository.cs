﻿using Elimination.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Elimination.Repositories
{
    public interface IEliminationRepository : IRepository<Models.Elimination>
    {
        IEnumerable<Models.Elimination> GetEliminationByID(int e);
    }
}
