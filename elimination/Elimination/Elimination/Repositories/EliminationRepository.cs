﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Elimination.Models;

namespace Elimination.Repositories
{
    public class EliminationRepository : Repository<Models.Elimination>, IEliminationRepository
    {
        public EliminationRepository(DbContext context) : base(context)
        {
        }

        IEnumerable<Models.Elimination> IEliminationRepository.GetEliminationByID(int e)
        {
            return EliminationDbContext.Eliminations.Where(pb => pb.EEliminator.PlayerID.Equals(e)).ToList();
        }

        public EliminationDBContext EliminationDbContext => Context as EliminationDBContext;
    }
}