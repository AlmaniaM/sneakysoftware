﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Elimination.Models;
using Elimination.Repositories.IRepositories;

namespace Elimination.Repositories
{
    public class RepoManager : IRepoManager
    {
        private readonly EliminationDBContext _eliminationDbContext;

        public RepoManager()
        {
            _eliminationDbContext = new EliminationDBContext();
            Badges = new BadgeRepository(_eliminationDbContext);
            Games = new GameRepository(_eliminationDbContext);
            Players = new PlayerRepository(_eliminationDbContext);
            PlayersLists = new PlayersListRepository(_eliminationDbContext);
            Eliminations = new EliminationRepository(_eliminationDbContext);
            PlayerBadges = new PlayerBadgeRepository(_eliminationDbContext);
            Users = new AspNetRepository(_eliminationDbContext);
            Targets = new TargetRepository(_eliminationDbContext);
        }

        public IAspNetRepository Users { get; private set; }
        public IBadgeRepository Badges { get; private set; }
        public IGameRepository Games { get; private set; }
        public IPlayerRepository Players { get; private set; }
        public IPlayersListRepository PlayersLists { get; private set; }
        public IEliminationRepository Eliminations { get; private set; }
        public IPlayerBadgeRepository PlayerBadges { get; private set; }
        public ITargetRepository Targets { get; private set; }

        public string GetBadgeNameById(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveDb()
        {
            return _eliminationDbContext.SaveChanges();
        }

        public async Task<int> SaveDbAsync()
        {
            return await _eliminationDbContext.SaveChangesAsync();
        }

        public void Dispose()
        {
            _eliminationDbContext.Dispose();
        }

        public DbContextTransaction BeginTransaction()
        {
            return _eliminationDbContext.Database.BeginTransaction();
        }
    }
}
