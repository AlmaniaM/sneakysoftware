﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using Elimination.Models;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using Microsoft.Data.OData.Query.SemanticAst;
using Microsoft.Owin.Security.Provider;

namespace Elimination.Repositories
{
    public class PlayerRepository : Repository<Player>, IPlayerRepository
    {
        public PlayerRepository(DbContext context) : base(context)
        {
        }

        public Player GetPlayerByID(int gId) => EliminationDbContext.Players.FirstOrDefault(y => y.PlayerID.Equals(gId));

        public Task<Player> GetPlayerByIdAsync(int p) =>
            EliminationDbContext.Players.FirstOrDefaultAsync(player => player.PlayerID == p);

        public Task<Player> awaitGetPlayerByID(int p) => EliminationDbContext.Players.FirstOrDefaultAsync(player => player.PlayerID == p);

        public IEnumerable<Player> GetPlayers() => EliminationDbContext.Players.ToList();

        public IEnumerable<Player> GetPlayersFriends(Player p) => EliminationDbContext.Players.FirstOrDefault(player => player.PlayerID == p.PlayerID)?.PFFriends;

        public IEnumerable<Player> GetPlayersNonFriends(Player p)
        {
            var players = EliminationDbContext.Players.ToList();
            foreach (var friend in p.PFFriends)
            {
                players.Remove(friend);
            }

            players.Remove(p);
            return players;
        }

        public Player GetPlayerByAuthId(string id) => EliminationDbContext.Players.FirstOrDefault(p => p.AuthUserID == id);

        public Models.Elimination GetMostRecentElimination(Player p) => p.EliminationsEliminated.FirstOrDefault();

        public Player GetMostRecentPlayersEliminatedBy(Player p) => p.EliminationsEliminated.FirstOrDefault()?.EEliminated;

        public Player GetMostRecentEliminatorOf(Player p) => EliminationDbContext.Eliminations
            .Select(el => el.EEliminator).FirstOrDefault(player => player.PlayerID == p.PlayerID);

        public IEnumerable<Player> GetTargetsEliminatedBy(Player p) =>
            EliminationDbContext.Eliminations.Where(e => e.EEliminator.PlayerID == p.PlayerID).Select(el => el.EEliminated).ToList();

        public IEnumerable<Player> GetPlayersWhoEliminated(Player p) => EliminationDbContext.Eliminations
            .Where(e => e.EEliminated.PlayerID == p.PlayerID).Select(el => el.EEliminator);

        public int HowManyTimesHasPlayerBeenEliminated(Player player) => EliminationDbContext.Eliminations.Count(e => e.Eliminated == player.PlayerID);

        public int HowManyTargetsHasPlayerEliminated(Player player) => EliminationDbContext.Eliminations.Count(e => e.Eliminator == player.PlayerID);

        public int HowManyTimesHasPlayerEliminatedTarget(Player player, Player target)
        {
            var targetTags = EliminationDbContext.Eliminations.Where(e => e.Eliminated == target.PlayerID);
            return targetTags.Count(tt => tt.EEliminator.PlayerID == player.PlayerID);
        }
       
        public IEnumerable<int> GetPlayersTarget(int p)
        {
            Player player = EliminationDbContext.Players.Where(x => x.PlayerID.Equals(p)).First();
            List<Game> games = player.PlayersGame.ToList();
            Game mostRecentGame = games.LastOrDefault();
            return EliminationDbContext.Targets.Where(x => x.Player.Equals(p) && x.GameID.Equals(mostRecentGame.GameID)).Select(x => x.TargetID);
            }

        public IEnumerable<int> GetGameID(int pID, int tID) => EliminationDbContext.Targets.Where(x => x.Player.Equals(pID) && x.TargetID.Equals(tID)).Select(x => x.GameID);
        
        /// <summary>
        /// Returns the highest number of times this player has eliminated a certain player
        /// </summary>
        /// <param name="player"></param>
        /// <returns></returns>
        public int HowManyTimesHasPlayerEliminatedSameTarget(Player player)
        {
            var sameTargetCount = EliminationDbContext.Eliminations.Where(e => e.Eliminator == player.PlayerID)
                .GroupBy(e2 => e2.Eliminated).Select(e3 => new {count = e3.Count()});

            var targetCount = 0;
            if (sameTargetCount.Any())
                targetCount = sameTargetCount.Max(e => e.count);

            return targetCount;
        }

        public int GetPlayerIDyByEmail(String email) => EliminationDbContext.Players.Where(x => x.Email.Equals(email)).Select(x => x.PlayerID).FirstOrDefault();

        public EliminationDBContext EliminationDbContext => Context as EliminationDBContext;

        internal Player Get(int? id) => EliminationDbContext.Players.Find(id);
    }
}
