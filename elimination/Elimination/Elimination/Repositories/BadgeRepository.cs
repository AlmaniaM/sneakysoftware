﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Elimination.Models;

namespace Elimination.Repositories
{
    public class BadgeRepository : Repository<Badge>, IBadgeRepository 
    {
        public BadgeRepository(DbContext context) : base(context)
        {
        }

        public IEnumerable<Badge> GetBadgesByPlayer(Player p)
        {
            return EliminationDbContext.PlayerBadges.Where(pb => pb.Player.PlayerID == p.PlayerID).Select(b => b.Badge).ToList();
        }

        public IEnumerable<Badge> GetBadgesByGame(Game g)
        {
            return EliminationDbContext.PlayerBadges.Where(pb => pb.GameID == g.GameID).Select(b => b.Badge).ToList();
        }

        public Badge GetBadgeByName(string badgeName)
        {
            return EliminationDbContext.Badges.FirstOrDefault(b => b.BadgeName.Equals(badgeName));
        }

        public IEnumerable<Badge> GetBadgesById(int g)
        {
            return EliminationDbContext.Badges.Where(pb => pb.BadgeID == g).ToList();
        }

        public bool Badge(int pID, int bID, int gID = 0)
        {
            var list = EliminationDbContext.PlayerBadges.Where(x => x.Badge.BadgeID.Equals(bID) && x.Player.PlayerID.Equals(pID));
            if (list.Any() != true)
            {
                if(bID < 1||pID < 1|| gID < 0)
                {
                    return false;
                }
                PlayerBadge b = new PlayerBadge { PlayerID = pID, BadgeID = bID, DateEarned = DateTime.Today, GameID = gID };
                EliminationDbContext.PlayerBadges.Add(b);
                return  true;
            }
            return false;
        }
        public EliminationDBContext EliminationDbContext => Context as EliminationDBContext;
    }
}