﻿USE [Elimination]

/*********************Start of down script************************/

/*Drop the Players table*/
IF OBJECT_ID('dbo.Players', 'U') IS NOT NULL
BEGIN TRY
	IF(OBJECT_ID('FK_PlayerBadges_Player', 'F') IS NOT NULL)
	BEGIN
		ALTER TABLE [PlayerBadges] DROP CONSTRAINT [FK_PlayerBadges_Player]
	END
	IF(OBJECT_ID('FK_FriendRequest_Player', 'F') IS NOT NULL)
	BEGIN
		ALTER TABLE [FriendRequests] DROP CONSTRAINT [FK_FriendRequest_Player]
	END
	IF(OBJECT_ID('FK_FriendRequest_Requester', 'F') IS NOT NULL)
	BEGIN
		ALTER TABLE [FriendRequests] DROP CONSTRAINT [FK_FriendRequest_Requester]
	END
	IF(OBJECT_ID('FK_Eliminations_Eliminator', 'F') IS NOT NULL)
	BEGIN
		ALTER TABLE [Eliminations] DROP CONSTRAINT [FK_Eliminations_Eliminator]
	END
	IF(OBJECT_ID('FK_Eliminations_Eliminated', 'F') IS NOT NULL)
	BEGIN
		ALTER TABLE [Eliminations] DROP CONSTRAINT [FK_Eliminations_Eliminated]
	END
	IF(OBJECT_ID('FK_Attempts_Eliminator', 'F') IS NOT NULL)
	BEGIN
		ALTER TABLE [Attempts] DROP CONSTRAINT [FK_Attempts_Eliminator]
	END
	IF(OBJECT_ID('FK_Attempts_Eliminated', 'F') IS NOT NULL)
	BEGIN
		ALTER TABLE [Attempts] DROP CONSTRAINT [FK_Attempts_Eliminated]
	END
	IF(OBJECT_ID('FK_Targets_Player', 'F') IS NOT NULL)
	BEGIN
		ALTER TABLE [Targets] DROP CONSTRAINT [FK_Targets_Player]
	END
	IF(OBJECT_ID('FK_Targets_Target', 'F') IS NOT NULL)
	BEGIN
		ALTER TABLE [Targets] DROP CONSTRAINT [FK_Targets_Target]
	END
	IF(OBJECT_ID('FK_PlayersLists_PlayerID', 'F') IS NOT NULL)
	BEGIN
		ALTER TABLE [PlayersLists] DROP CONSTRAINT [FK_PlayersLists_PlayerID]
	END
	IF(OBJECT_ID('FK_PlayersFriends_Player', 'F') IS NOT NULL)
	BEGIN
		ALTER TABLE [PlayersFriends] DROP CONSTRAINT [FK_PlayersFriends_Player]
	END
	IF(OBJECT_ID('FK_PlayersFriends_Friend', 'F') IS NOT NULL)
	BEGIN
		ALTER TABLE [PlayersFriends] DROP CONSTRAINT [FK_PlayersFriends_Friend]
	END
	IF(OBJECT_ID('FK_RequestingPlayers_PlayerID', 'F') IS NOT NULL)
	BEGIN
		ALTER TABLE [RequestingPlayers] DROP CONSTRAINT [FK_RequestingPlayers_PlayerID]
	END
	IF(OBJECT_ID('FK_Host_HostID', 'F') IS NOT NULL)
	BEGIN
		ALTER TABLE [Games] DROP CONSTRAINT [FK_Host_HostID]
	END
	DROP TABLE IF EXISTS [Players];
END TRY
BEGIN CATCH
	PRINT 'Could not delete Players table';
END CATCH;
GO


/*Drop the Games table*/
IF OBJECT_ID('dbo.Games', 'U') IS NOT NULL
BEGIN TRY 
	IF(OBJECT_ID('FK_PlayersLists_GameID', 'F') IS NOT NULL)
	BEGIN
		ALTER TABLE [PlayersLists] DROP CONSTRAINT [FK_PlayersLists_GameID]
	END
	IF(OBJECT_ID('FK_RequestingPlayers_GameID', 'F') IS NOT NULL)
	BEGIN
		ALTER TABLE [RequestingPlayers] DROP CONSTRAINT [FK_RequestingPlayers_GameID]
	END
	IF(OBJECT_ID('FK_Eliminations_GameID', 'F') IS NOT NULL)
	BEGIN
		ALTER TABLE [Eliminations] DROP CONSTRAINT [FK_Eliminations_GameID]
	END
	IF(OBJECT_ID('FK_Targets_GameID', 'F') IS NOT NULL)
	BEGIN
		ALTER TABLE [Targets] DROP CONSTRAINT [FK_Targets_GameID]
	END
	IF(OBJECT_ID('FK_Attempts_GameID', 'F') IS NOT NULL)
	BEGIN
		ALTER TABLE [Attempts] DROP CONSTRAINT [FK_Attempts_GameID]
	END
	DROP TABLE IF EXISTS [Games]
END TRY
BEGIN CATCH
	PRINT 'Could not delete Game table';
END CATCH;
GO

/*Drop the FriendRequests table*/
IF OBJECT_ID('dbo.FriendRequests', 'U') IS NOT NULL
BEGIN TRY 
	DROP TABLE IF EXISTS [FriendRequests];
END TRY
BEGIN CATCH
	PRINT 'Could not delete FriendRequests table';
END CATCH;
GO

/*Drop the PlayersLists table*/
IF OBJECT_ID('dbo.PlayersLists', 'U') IS NOT NULL
BEGIN TRY 
	DROP TABLE IF EXISTS [PlayersLists];
END TRY
BEGIN CATCH
	PRINT 'Could not delete PlayersLists table';
END CATCH;
GO

/*Dropt the Eliminations table*/
IF OBJECT_ID('dbo.Eliminations', 'U') IS NOT NULL
BEGIN TRY 
	DROP TABLE IF EXISTS [Eliminations];
END TRY
BEGIN CATCH
	PRINT 'Could not delete Eliminations table';
END CATCH;
GO

/*Drop the Targets table*/
IF OBJECT_ID('dbo.Targets', 'U') IS NOT NULL
BEGIN TRY 
	DROP TABLE IF EXISTS [Targets];
END TRY
BEGIN CATCH
	PRINT 'Could not delete Targets table';
END CATCH;
GO

/*Drop the Attempts table*/
IF OBJECT_ID('dbo.Attempts', 'U') IS NOT NULL
BEGIN TRY 
	DROP TABLE IF EXISTS [Attempts];
END TRY
BEGIN CATCH
	PRINT 'Could not delete Attempts table';
END CATCH;
GO

/*Drop the PlayersFriends table*/
IF OBJECT_ID('dbo.PlayersFriends', 'U') IS NOT NULL
BEGIN TRY 
	DROP TABLE IF EXISTS [PlayersFriends];
END TRY
BEGIN CATCH
	PRINT 'Could not delete PlayersFriends table';
END CATCH;
GO

/*Drop the RequestingPlayers table*/
IF OBJECT_ID('dbo.RequestingPlayers', 'U') IS NOT NULL
BEGIN TRY 
	DROP TABLE IF EXISTS [RequestingPlayers];
END TRY
BEGIN CATCH
	PRINT 'Could not delete RequestingPlayers table';
END CATCH;
GO

/*Drop the Badges table*/
IF OBJECT_ID('dbo.Badges', 'U') IS NOT NULL
BEGIN TRY 
	IF(OBJECT_ID('FK_PlayerBadges_Badge', 'F') IS NOT NULL)
	BEGIN
		ALTER TABLE [PlayerBadges] DROP CONSTRAINT [FK_PlayerBadges_Badge]
	END
	DROP TABLE IF EXISTS [Badges];
END TRY
BEGIN CATCH
	PRINT 'Could not delete Badges table';
END CATCH;
GO

/*Drop the PlayerBadges table*/
IF OBJECT_ID('dbo.PlayerBadges', 'U') IS NOT NULL
BEGIN TRY 
	DROP TABLE IF EXISTS [PlayerBadges];
END TRY
BEGIN CATCH
	PRINT 'Could not delete PlayerBadges table';
END CATCH;
GO


/*********************End of down script************************/

