-- Exported from QuickDBD: https://www.quickdatatabasediagrams.com/
-- Link to schema: https://app.quickdatabasediagrams.com/#/schema/RVPOZ4sM_k-Bhk237T6VpA
-- NOTE! If you have used non-SQL datatypes in your design, you will have to change these here.

IF NOT EXISTS (SELECT * FROM sys.databases WHERE name = 'Elimination')
	CREATE DATABASE [Elimination]
GO

USE [Elimination];
GO


/*********************Start of up script************************/
SET XACT_ABORT ON

BEGIN TRANSACTION QUICKDBD

/*Table that hold information about a user/plaayer*/
CREATE TABLE [Players] (
    [PlayerID]		INT IDENTITY(1,1)	NOT NULL
    ,[FirstName]	NVARCHAR(64)		NOT NULL
    ,[LastName]		NVARCHAR(64)		NOT NULL
    ,[UserName]		NVARCHAR(64)		NOT NULL
    ,[Email]		NVARCHAR(96)		NOT NULL
    ,[Phone]		NVARCHAR(20)		NOT NULL
    ,[DOB]			DATETIME			NOT	NULL
    ,[PhotoUrl]		VARCHAR(max)		NULL
    ,[Profile]		NVARCHAR(256)		NOT NULL
	,[AuthUserID]	NVARCHAR(128)		NOT NULL
    ,CONSTRAINT [PK_Players] PRIMARY KEY CLUSTERED ([PlayerID] ASC)
)

/*Table that holds information about current and past games*/
CREATE TABLE [Games] (
    [GameID]				INT IDENTITY(1,1)	NOT NULL
    ,[Name]					NVARCHAR(64)				NOT NULL
	,[Host]					INT							NOT NULL
	,[Active]				BIT							NOT NULL
	,[AccesLevel]			BIT							NULL
    ,[StartDate]			DATETIME					NOT NULL
    ,[EndDate]				DATETIME					NULL
	,[Winner]				INT							NULL
    ,[Location]				NVARCHAR(64)				NOT NULL
    ,[LocationCenterLong]	decimal(9,6)				NOT NULL
    ,[LocatoinCenterLat]	DECIMAL(9,6)				NOT NULL
    ,[LocationRadius]		INT							NOT NULL
    ,CONSTRAINT [PK_Games] PRIMARY KEY CLUSTERED ([GameID] ASC)
	,CONSTRAINT [FK_Winner_PlayerID] FOREIGN KEY([Winner]) REFERENCES [Players] ([PlayerID])
	,CONSTRAINT [FK_Host_HostID] FOREIGN KEY([Host]) REFERENCES [Players] ([PlayerID])
)

/*Table that holds a list of a players' friends*/
CREATE TABLE [PlayersFriends] (
    [Player]			INT  NOT NULL
    ,[PlayerFiend]		INT  NOT NULL
	,CONSTRAINT [PK_PlayersFriends] PRIMARY KEY CLUSTERED ([Player], [PlayerFiend])
	,CONSTRAINT [FK_PlayersFriends_Player] FOREIGN KEY([Player])REFERENCES [Players] ([PlayerID])
	,CONSTRAINT [FK_PlayersFriends_Friend] FOREIGN KEY([PlayerFiend])REFERENCES [Players] ([PlayerID])
)

/*Table that holds friend requests*/
CREATE TABLE [FriendRequests] (
    [Player]			INT  NOT NULL
    ,[FriendRequestPlayer]		INT  NOT NULL
	,CONSTRAINT [PK_FriendRequests] PRIMARY KEY CLUSTERED ([Player], [FriendRequestPlayer])
	,CONSTRAINT [FK_FriendRequest_Player] FOREIGN KEY([Player])REFERENCES [Players] ([PlayerID])
	,CONSTRAINT [FK_FriendRequest_Requester] FOREIGN KEY([FriendRequestPlayer])REFERENCES [Players] ([PlayerID])
)

/*Table that holds players who are requesting to joing a game*/
CREATE TABLE [RequestingPlayers] (
    [GameID]				INT  NOT NULL
    ,[RequestingPlayer]		INT  NOT NULL
	,CONSTRAINT [PK_RequestingPlayers] PRIMARY KEY CLUSTERED ([GameID], [RequestingPlayer])
	,CONSTRAINT [FK_RequestingPlayers_GameID] FOREIGN KEY([GameID]) REFERENCES [Games] ([GameID])
	,CONSTRAINT [FK_RequestingPlayers_PlayerID] FOREIGN KEY([RequestingPlayer]) REFERENCES [Players] ([PlayerID])
)

/*Table that holds elimination recors from current and previous games*/
CREATE TABLE [Eliminations] (
    [EliminationID]			INT IDENTITY(1,1)	NOT NULL
    ,[Eliminator]			INT							NOT NULL
    ,[Eliminated]			INT							NOT NULL
    ,[LocationCenterLong]	DECIMAL(9,6)				NOT NULL
    ,[LocatoinCenterLat]	DECIMAL(9,6)				NOT NULL
    ,[TimeStamp]			DATETIME					NOT NULL
    ,[GameID]				INT							NOT NULL
    ,CONSTRAINT [PK_Eliminations] PRIMARY KEY CLUSTERED ([EliminationID] ASC)
	,CONSTRAINT [FK_Eliminations_Eliminator] FOREIGN KEY([Eliminator]) REFERENCES [Players] ([PlayerID])
	,CONSTRAINT [FK_Eliminations_Eliminated] FOREIGN KEY([Eliminated]) REFERENCES [Players] ([PlayerID])
	,CONSTRAINT [FK_Eliminations_GameID] FOREIGN KEY([GameID]) REFERENCES [Games] ([GameID])
)

/*Table that holds a list of players in a given game*/
CREATE TABLE [PlayersLists] (
    [GameID]		INT  NOT NULL
    ,[PlayerID]		INT  NOT NULL
	,[TargetCode]	INT	 NULL
	,CONSTRAINT [PK_PlayersLists] PRIMARY KEY CLUSTERED ([GameID], [PlayerID])
	,CONSTRAINT [FK_PlayersLists_GameID] FOREIGN KEY([GameID]) REFERENCES [Games] ([GameID])
	,CONSTRAINT [FK_PlayersLists_PlayerID] FOREIGN KEY([PlayerID]) REFERENCES [Players] ([PlayerID])
)

/*Table that holds a given players target in a certain game*/
CREATE TABLE [Targets] (
    [GameID] INT  NOT NULL
    ,[Player] INT  NOT NULL
    ,[Target] INT  NOT NULL
	,CONSTRAINT [PK_Targest] PRIMARY KEY CLUSTERED ([GameID], [Player], [Target])
	,CONSTRAINT [FK_Targets_GameID] FOREIGN KEY([GameID]) REFERENCES [Games] ([GameID])
	,CONSTRAINT [FK_Targets_Player] FOREIGN KEY([Player]) REFERENCES [Players] ([PlayerID])
	,CONSTRAINT [FK_Targets_Target] FOREIGN KEY([Target]) REFERENCES [Players] ([PlayerID])
)

/*Table that holds records of players' attemps at a kill*/
CREATE TABLE [Attempts] (
    [AttemptID]				INT IDENTITY(1,1)			NOT NULL
    ,[Eliminator]			INT							NOT NULL
    ,[Eliminated]			INT							NOT NULL
    ,[LocationCenterLong]	DECIMAL(9,6)				NOT NULL
    ,[LocatoinCenterLat]	DECIMAL(9,6)				NOT NULL
    ,[TimeStamp]			DATETIME					NOT NULL
    ,[GameID]				INT							NOT NULL
    ,CONSTRAINT [PK_Attempts] PRIMARY KEY CLUSTERED ([AttemptID] ASC)
	,CONSTRAINT [FK_Attempts_Eliminator] FOREIGN KEY([Eliminator]) REFERENCES [Players] ([PlayerID])
	,CONSTRAINT [FK_Attempts_Eliminated] FOREIGN KEY([Eliminated]) REFERENCES [Players] ([PlayerID])
	,CONSTRAINT [FK_Attempts_GameID] FOREIGN KEY([GameID]) REFERENCES [Games] ([GameID])
)

/*Table that holds Badges*/
CREATE TABLE [Badges] (
	[BadgeID]				INT IDENTITY(1,1)			NOT NULL
	,[BadgeName]			VARCHAR(50)					NOT NULL
	,[BadgeRequirement]		INT							NULL
	,[BadgeDescription]		VARCHAR(100)				NULL
	,[Url]					VARCHAR(MAX)				NULL
	,CONSTRAINT [PK_Badges] PRIMARY KEY CLUSTERED ([BadgeID])
)

/*Table for badges earned by player*/
CREATE TABLE [PlayerBadges] (
	[PlayerID]				INT NOT NULL
	,[BadgeID]				INT NOT NULL
	,[GameID]				INT NULL
	,[DateEarned]			DATETIME NOT NULL
	,CONSTRAINT [PK_PlayersBadges] PRIMARY KEY ([PlayerID], [BadgeID])
	,CONSTRAINT [FK_PlayerBadges_Player] FOREIGN KEY([PlayerID]) REFERENCES [Players] ([PlayerID])
	,CONSTRAINT [FK_PlayerBadges_Badge] FOREIGN KEY([BadgeID]) REFERENCES [Badges] ([BadgeID])
)


COMMIT TRANSACTION QUICKDBD